defmodule EvallistTest do
  use ExUnit.Case
  doctest Evallist

  test "init evallist module" do
       assert Evallist.init()
  end

  test "Start receiving msgs" do
       assert Evallist.get_command()
  end

  test "get eval list" do
        Evallist.init()
       assert Evallist.get()
  end

  test "test add id to list" do
        Evallist.init()
       assert Evallist.add("1234")
       assert IO.write("#{}", Evallist.get())
  end
end
