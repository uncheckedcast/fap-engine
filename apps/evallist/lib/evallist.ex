defmodule Evallist do
  @moduledoc """
  Stores the id's and rules of the objects that the node is responsible for updating.
  """

  @doc """
  Starts the storage of id's and rules.
  """
  @spec init() :: atom()
  def init() do
    pid = spawn(fn -> get_command([]) end)
    Process.register(pid, :fapevallist)
  end
  
  def get_command(list) do
    receive do 
      {:add, obj} ->
	send({:listener,Node.self},{:count_change,length(list)+1})
	get_command([obj | list])
      {:rem, key} ->
	send({:listener,Node.self},{:count_change,length(list)-1})
	get_command(Enum.reject(list, fn (lobj) -> match_key(key, lobj) end))
      {:get, pid} ->
	send(pid, {:ok, list})
	get_command(list)
      {:get_route, pid} ->
	Faplogger.err(:evallist, "SENDS DUMB STUFF")
	send(pid,{:listen,{:evallist,list}})
	get_command([])
      o -> Faplogger.err(:evallist, "Got malformed message: #{inspect(o)}")
    end
  end
  
  def match_key(key, obj) do
    case obj do
      {^key, _} -> true
      _ -> false
    end
  end

  def add(obj) do
    #Faplogger.log(:evallist, "adding to evallist #{inspect(Node.self())}, object- #{inspect(obj)}")
    send(:fapevallist, {:add, obj})
    :ok
  end

  def remove(key) do
    #Faplogger.log(:evallist, "removing from evallist #{inspect(Node.self())}, object- #{inspect(key)}")
    send(:fapevallist, {:rem, key})
  end

  def get() do
    send(:fapevallist, {:get, self()})
    receive do
      {:ok, list} -> list
      _ -> []
    end
  end

  def obj_rule_parse(obj) do
    {{:pos, {xPos, yPos, zPos}},
     {:vectors, posVectors=[{x0,y0,z0}, {x1, y1, z1}, {x2, y2, z2}]},
     {:speed, {xVel, yVel, zVel}},
     {:resultingForce, {xFor, yFor, zFor}}, {:type, objType},
     {:attributes, attributes}, {:rules, rules}} = obj
    rule_list_parser(rules)
  end

  def rule_list_parser([]) do [] end
  def rule_list_parser([h|t] = rules) do
    [Evaluator.Parser.parse(h)|rule_list_parser(t)]
  end

end
