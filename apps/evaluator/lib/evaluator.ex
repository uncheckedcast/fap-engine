# User story: FAPlang implementation ID-B04
#
# Evaluator for FAPlang
# The evaluator expects a correctly built tuple/list-tree that is parsed in Evaluator.parser
# To see an example of a correctly built rule-list used as argument please see the test/0 function
# The FAPlang scriptfiles will be named the following: *.fl
#
# If evaluation fails it drops the current evalution and leaves error message.
#
# For any further questions please contact the team with the email adress found on our project website!
# Link to project website: https://uncheckedcast.gitlab.io/fap/
#------------------------------------------------------------------------------------------------------------
defmodule Evaluator do
  # All instructions will start with the :on keyword

  @doc """
  Method eval_rules/3, takes the following three arguments:
  ruleList = [{rule1}, {rule2}, ..., {ruleN}] - See test/0 if you need better insight in formatting.
  env = [this: objA, other: objB] - where objA might have the following structure:
  objA = [key: "key"]
  event = :event - where the atom :event can be switched out for right event
  Expected outcome: entire ruleList is evaluated withouth runtime errors
  The clause below is used for quickly terminating evaluation if the ruleList is empty
  """
  @spec eval_rules(list(), list(), atom()) :: any()
  def eval_rules([], _, env) do
    Faplogger.err(:evaluator, "empty rule list!!!!")
    :failure
  end
  def eval_rules([h | t] = _ruleList, env, event) do
    # Match the tail of the ruleList to see if it is empty or not
    case t do
      [] ->
        # Evaluate the last rule in the ruleList, terminate the recursive call
        eval(h, env, event)
      _ ->
        # Evaluate the first rule in the ruleList, with argument environment and event
        updatedEnv = eval(h, env, event)
        # Recursively iterate through the ruleList so that every rule is evaluated
        eval_rules(t, updatedEnv, event)
    end
  end

  @doc """
  Method eval_args/2 takes a list of arguments and an environment list
  Expected outcome: a list of the all the arguments evaluated
  """
  @spec eval_args(list(), list()) :: list()
  def eval_args([], _env) do [] end
  # Func clause to build the argument list, recursively builds it in the same order as the arguments were given
  def eval_args([h | t], env) do
    [eval(h, env) | eval_args(t, env)]
  end

  @doc """
  Method eval/3 takes rule argument, environment argument and event argument
  Expected outcome: evaluating the right status keyword and evaluating rest
  """
  @spec eval(tuple(), list(), atom()) :: any()
  def eval({status, e, body} = _parsedMsg, env, event) do
    # Match status with available keywords
    case status do
      :on ->
        eval({e, body}, env, event)
      # If don't care is triggered, there was a faulty message formatting
      _ ->
        Faplogger.err(:evaluator, "Incorrect faplang syntax - #{inspect(status)}")
        :failure
    end
  end
  @doc """
  Method eval/3 takes an event and body argument, environment argument and event argument
  Expected outcome: matching the internal rule-event with the specified event
  """
  @spec eval(tuple(), list(), atom()) :: any()
  def eval({{:event, e}, body}, env, event) do
    # Match internal rule-event with available events for current runtime, i.e. the event argument
    case e do
      ^event ->
        # Internal rule-event is correct, evaluate the body of the rule with the given environment
        eval(body, env)
      # If don't care was triggered, there was a faulty internal rule-event specified
      _ ->
        env
    end
  end

  @doc """
  Method eval/2 takes a body argument and an environment argument
  Expected outcome: entire body is evaluated within the given environment without runtime errors
  The clause below is used for directly terminating evaluation if the body is empty
  """
  @spec eval(tuple(), list()) :: any()
  def eval({:do, []}, _env) do
    Faplogger.err(:evaluator, "Failed to evaluate do clause - empty body")
    :failure
  end
  def eval({:do, [h | t] = _body}, env) do
    case t do
      [] ->
        eval(h, env)
      _ ->
        case eval(h, env) do
          :failure ->
            eval({:do, t}, env)
          updatedEnv ->
            eval({:do, t}, updatedEnv)
        end
    end
  end
  @doc """
  Method eval/2 takes object & expression tuple argument and environment argument
  Expected outcome: the object being evaluated and assigned to whatever the expression is evaluated to
  """
  @spec eval(tuple(), list()) :: any()
  def eval({:put, {objname, key = _attribute} = _obj, exp}, env) do
    # Try and find the object argument in the given environment
    case List.keyfind(env, objname, 0, :error) do
      # The requested object is NOT present in the environment, i.e. not part of rule
      :error ->
        Faplogger.err(:evaluator, "Failure object name not found - #{inspect(objname)}")
        :failure
      # The requested object is present in the environment
      {_, obj} ->
        # Assign the evaluated expression to the object and returns the upated environment list
        upd = Map.put(obj, key, eval(exp, env))
        List.keystore(env, objname, 0, {objname, upd})
    end
  end
  @doc """
  Method eval/2 takes an object argument and an environment argument
  Expected outcome: the value assigned to the key of requested object that is present in the environment
  """
  @spec eval(tuple(), list()) :: any()
  def eval({:get, {objname, key = _attribute} = _obj}, env) do
    # Try and find the object argument in the given environment
    case List.keyfind(env, objname, 0, :error) do
      # The requested object is NOT present in the environment
      :error ->
        Faplogger.err(:evaluator, "Failure object name not found - #{inspect(objname)}")
        :failure
      # The requested object is present in the environment
      {_, obj} ->
        # Try and find the key-string/attribute argument in the above-found object
        case Map.fetch(obj, key) do
          # The requested key-string/attribute is NOT present in the object structure
          :error ->
            Faplogger.err(:evaluator, "Failure key not found - #{inspect(key)}")
            :failure
          # The requested key-string/attribute is present in the object structure
          {:ok, value} ->
            # Return the value associated with the key-string/attribute
            value
        end
    end
  end
  @doc """
  Method eval/2 takes an expression & matchclauses tuple argument and an environment argument
  Expected outcome: the expression argument is matched with one of the matchclauses and evaluated
  """
  @spec eval(tuple(), list()) :: any()
  def eval({:match, _exp, []}, _env) do
    Faplogger.err(:evaluator, "Failure no matchclauses")
    :failure
  end
  def eval({:match, exp, [h | t] = _clauseList}, env) do
    # Try and retrieve the first matchclause in the clauseList
    case h do
      # The matchclause at the front of the list is formatted correctly
      {:matchclause, condition, {:do, body}} ->
        case eval(condition, env) do
          {:less, n} ->
            case eval({:less, exp, n}, env) do
              :true ->
                eval_body(body, env)
              :false ->
                case t do
                  [] ->
                    env
                  _ ->
                    eval({:match, exp, t}, env)
                end
            end
          {:greater, n} ->
            case eval({:greater, exp, n}, env) do
              :true ->
                eval_body(body, env)
              :false ->
                case t do
                  [] ->
                    env
                  _ ->
                    eval({:match, exp, t}, env)
                end
            end
          :kbry ->
            eval_body(body, env)
          tmp ->
            case eval(exp, env) do
              ^tmp ->
                eval_body(body, env)
              _ ->
                case t do
                  [] ->
                    env
                  _ ->
                    eval({:match, exp, t}, env)
                end
            end
        end
      # The matchclause was faulty formatted and thus not matched with the previous clause
      _ ->
        Faplogger.err(:evaluator, "Head of body did not match correct structure, h - #{inspect(h)}")
        :failure
    end
  end

  def eval_body([], _env) do
    Faplogger.err(:evaluator, "Failure empty body")
    :failure
  end
  def eval_body([h | t], env) do
    case t do
      [] ->
        eval(h, env)
      _ ->
        case eval(h, env) do
          :failure ->
            Faplogger.log(:evaluator, "Failed to evaluate body - #{inspect(h)}")
            :failure
          updatedEnv ->
            eval_body(t, updatedEnv)
        end
    end
  end
  @doc """
  Method eval/2 takes a 'less' instruction and two parameters and an environment argument
  Expected outcome: either the boolean 'true' or 'false' depending on if the first parameter is less or equal to the second one
  """
  @spec eval(tuple(), list()) :: atom()
  def eval({:less, expL, expR}, env) do
    cond do
      # Compare the evaluated result of the two parameter arguments. EXPECTED evaluation is a float
      eval(expL, env) <= eval(expR, env) ->
        :true
      true ->
        :false
    end
  end
  @doc """
  Method eval/2 takes a 'greater' instruction and two parameters and an environment argument
  Expected outcome: either the boolean 'true' or 'false' depending on if the first parameter is greater or equal to the second one
  """
  @spec eval(tuple(), list()) :: atom()
  def eval({:greater, expL, expR}, env) do
    cond do
      # Compare the evaluated result of the two parameter arguments. EXPECTED evaluation is a float
      eval(expL, env) >= eval(expR, env) ->
        :true
      true ->
        :false
    end
  end
  def eval({:greater, n}, _env) do {:greater, n} end
  def eval({:less, n}, _env) do {:less, n} end
  @doc """
  Method eval/2 takes a 'func' instruction and a function with arguments to call
  Expected outcome: whatever the called function returns, use with caution!
  """
  @spec eval(tuple(), list()) :: any()
  def eval({:func, {module, func, args}}, env) do
    apply(String.to_existing_atom("Elixir.#{module}"), func, eval_args(args, env))
    env
  end
  @doc """
  Method eval/2 takes an adding instruction with two parameters and an environment argument
  Expected outcome: the result of adding the evaluation of the two parameters
  """
  @spec eval(tuple(), list()) :: float()
  def eval({:add, n, m}, env) do
    case eval(n, env) do
      :failure ->
        Faplogger.err(:evaluator, "Failed to evaluate - #{inspect(n)}")
        :failure
      evalN ->
        case eval(m, env) do
          :failure ->
            Faplogger.err(:evaluator, "Failed to evaluate - #{inspect(m)}")
            :failure
          evalM ->
            evalN + evalM
        end
    end
  end
  @doc """
  Method eval/2 takes a subtraction instruction with two parameters and an environment argument
  Expected outcome: the result of subtracting the evaluation of the second parameter from the evaluation of the first parameter
  """
  @spec eval(tuple(), list()) :: float()
  def eval({:sub, n, m}, env) do
    case eval(n, env) do
      :failure ->
        Faplogger.err(:evaluator, "Failed to evaluate - #{inspect(n)}")
        :failure
      evalN ->
        case eval(m, env) do
          :failure ->
            Faplogger.err(:evaluator, "Failed to evaluate - #{inspect(m)}")
            :failure
          evalM ->
            evalN - evalM
        end
    end
  end
  @doc """
  Method eval/2 takes a division instruction with two parameters and an environment argument
  Expected outcome: the result of dividing the evaluation of the first parameter with the evaluation of the second parameter
  """
  @spec eval(tuple(), list()) :: float()
  def eval({:div, n, m}, env) do
    case eval(n, env) do
      :failure ->
        Faplogger.err(:evaluator, "Failed to evaluate - #{inspect(n)}")
        :failure
      evalN ->
        case eval(m, env) do
          :failure ->
            Faplogger.err(:evaluator, "Failed to evaluate - #{inspect(m)}")
            :failure
          evalM ->
            evalN / evalM
        end
    end
  end
  @doc """
  Method eval/2 takes a multiplication instruction with two parameters and an environment argument
  Expected outcome: the result of multiplying the evaluation of the first parameter with the evaluation of the second parameter
  """
  @spec eval(tuple(), list()) :: float()
  def eval({:mul, n, m}, env) do
    case eval(n, env) do
      :failure ->
        Faplogger.err(:evaluator, "Failed to evaluate - #{inspect(n)}")
        :failure
      evalN ->
        case eval(m, env) do
          :failure ->
            Faplogger.err(:evaluator, "Failed to evaluate - #{inspect(m)}")
            :failure
          evalM ->
            evalN * evalM
        end
    end
  end
  @doc """
  Method eval/2 takes a float instruction and one parameter as argument and an environment argument
  Expected outcome: the float value of the parameter argument, dismisses the environment
  """
  @spec eval(tuple(), any()) :: float()
  def eval({:float, x}, _env) do x end
  @doc """
  Method eval/2 that takes a string instruction and one parameter as argument and an environment argument
  Expected outcome: the string datastructure representation of the parameter argument, dismisses the environment
  """
  @spec eval(tuple(), any()) :: String.t()
  def eval({:string, s}, _env) do s end
  @doc """
  Method eval/2 takes an undefined instruction with an error message argument and an environment argument
  Expected outcome: the error message being displayed in the shell for debugging purposes, dismisses the environment
  """
  @spec eval(tuple(), any()) :: fun()
  def eval({:undef, s}, _env) do Faplogger.err(:evaluator, "Undefined message - #{inspect(s)}") end
  @doc """
  Method eval/2 takes a 'don't care' instruction argument and an environment argument
  Expected outcome: returns the predefined atom for 'don't care' that is :kbry, dismisses the environment
  """
  @spec eval(tuple(), any()) :: atom()
  def eval({:kbry}, _env) do :kbry end

  ###########################################################################################################
  # Method test/0, calls eval_rules/3 with a preformated rule-list and expects to be evaluted completely
  # without any runtime errors.

  ## Method test/0 NOT PART OF FAPlang API, used for internal unit testing!
  def test() do
    # {:on, {:event, :tick}, {:do, [{:match, {:float, 3.0}, [{:matchclause, {:less, {:float, 1.0}}, {:mul, {:float, 2.0}, {:float, 3.0}}}, {:matchclause, {:less, {:float, 4.0}}, {:add, {:float, 2.0}, {:float, 3.0}}}]}]}}
    obj = {{:pos, {1.0, 1.0, 1.0}},
     {:vectors, [{1.0,1.0,1.0}, {1.1, 1.1, 1.1}, {1.2, 1.2, 1.2}]},
     {:speed, {1.1, 1.1, 1.1}},
     {:resultingForce, {0.0, 0.0, 0.0}}, {:type, "typ"},
     {:attributes, %{"health" => 10.0}}, {:rules, []}}
    obj2 = {{:pos, {1.0, 1.0, 1.0}},
     {:vectors, [{1.0,1.0,1.0}, {1.1, 1.1, 1.1}, {1.2, 1.2, 1.2}]},
     {:speed, {10.0, 20.0, 30.0}},
     {:resultingForce, {0.0, 0.0, 0.0}}, {:type, "typ"},
     {:attributes, %{"health" => 2.0, "damage" => 10.0, "boulderTime" => 10.0, "slowTime" => 10.0, "fireTime" => 2.0}}, {:rules, []}}
    eval_rules([Evaluator.Parser.test()], Evaluator.Parser.parse_env([{:world, %{"num_types" => 0.0}, "id"}, {:this, obj2, "id"}]), :tick)
  end
  ###########################################################################################################

end
#------------------------------------------------------------------------------------------------------------
