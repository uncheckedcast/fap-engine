defmodule Evaluator.Parser do

  @doc """
  Method parse_env/1 takes a list as argument, the list should contain tuples
  Expected outcome is a list with tuples
  """
  @spec parse_env(list()) :: list()
  def parse_env([]) do [] end
  def parse_env([{name, obj, id} | t] = _objlist) do
    case is_obj(obj) do
      true ->
        [{name, parse_env_obj(obj, id)}, {String.to_atom("#{Atom.to_string(name)}_attr"), parse_attrib(obj)} | parse_env(t)]
      false ->
        [{name, parse_env_obj(obj, id)} | parse_env(t)]
    end
  end

  @doc """
  Method parse_env_obj/2 takes an object as a tuple and a string id
  Or input map if meta-object
  Expected outcome is a Map
  """
  @spec parse_env_obj(tuple(), String.t()) :: list()
  def parse_env_obj(obj, id) do
    case obj do
      {{:pos, {xPos, yPos, zPos}},
       {:vectors, vecs},
       {:speed, {xVel, yVel, zVel}},
       {:resultingForce, {xFor, yFor, zFor}}, {:type, objType},
       {:attributes, _attributes}, {:rules, rules}} ->
        #[
        #  {:posX, xPos}, {:posY, yPos}, {:posZ, zPos},
        #  {:velX, xVel}, {:velY, yVel}, {:velZ, zVel},
        #  {:resFX, xFor}, {:resFY, yFor}, {:resFZ, zFor},
        #  {:type, objType}, {:id, id}, {:_vecs, vecs}, {:_rules, rules}
        #]
        %{
          "posX" => xPos, "posY" => yPos, "posZ" => zPos,
          "velX" => xVel, "velY" => yVel, "velZ" => zVel,
          "resFX" => xFor, "resFY" => yFor, "resFZ" => zFor,
          "type" => objType, "id" => id, "_vecs" => vecs, "_rules" => rules
        }
      _ ->
        obj
    end
  end

  @doc """
  Method parse_attrib/1 takes a list with tuples
  Expected outcome is a list with tuples, where the tuples represent different attributes for the objects
  """
  #@spec parse_attrib(list()) :: list()
  #def parse_attrib([]) do [] end
  #def parse_attrib([{tag, val} | t]) do [{tag, val} | parse_attrib(t)] end

  @doc """
  Method parse_attrib/1 takes an object as a tuple
  Expected outcome is a list, since the last function called returns a list
  """
  @spec parse_attrib(tuple()) :: list()
  def parse_attrib(obj) do
    {{:pos, {_xPos, _yPos, _zPos}},
       {:vectors, [{_x0,_y0,_z0}, {_x1, _y1, _z1}, {_x2, _y2, _z2}]},
       {:speed, {_xVel, _yVel, _zVel}},
       {:resultingForce, {_xFor, _yFor, _zFor}}, {:type, _objType},
       {:attributes, attributes}, {:rules, _rules}} = obj
    attributes
  end

  @doc """
  Method deparse_env/1 takes a list with objects that are used in the evaluation
  Expected outcome is an updated environment, that is, a list with tuples
  """
  @spec deparse_env(list()) :: list()
  def deparse_env([]) do [] end
  def deparse_env(env) do
    {curName, cur} = List.first(env)
    env = List.delete_at(env, 0)
    case List.keytake(env, String.to_atom("#{Atom.to_string(curName)}_attr"), 0) do
      {{_curAttrName, curAttr}, rest} ->
        #Do sometihing
        {id, obj} = deparse_obj(cur, curAttr)
        [{curName, obj, id} | deparse_env(rest)]
      nil ->
        #No attr
        [{curName, cur, ""} | deparse_env(env)]
    end
  end

  @doc """
  Method deparse_obj/2 takes an object and an enumerable as arguments
  Expected outcome is an updated obj, that is, a tuple-structure
  """
  @spec deparse_obj(tuple(), Enumerable.t()) :: tuple()
  def deparse_obj(obj, attr) do
    out = {{:pos, {Map.fetch!(obj, "posX"), Map.fetch!(obj, "posY"), Map.fetch!(obj, "posZ")}},
       {:vectors, Map.fetch!(obj, "_vecs")},
       {:speed, {Map.fetch!(obj, "velX"), Map.fetch!(obj, "velY"), Map.fetch!(obj, "velZ")}},
       {:resultingForce, {Map.fetch!(obj, "resFX"), Map.fetch!(obj, "resFY"), Map.fetch!(obj, "resFZ")}},
       {:type, Map.fetch!(obj, "type")},
       {:attributes, attr}, {:rules, Map.fetch!(obj, "_rules")}}
    {Map.fetch!(obj, "id"), out}
  end

  @doc """
  Method is_obj/1 takes an object and checks if it follows the correct structure
  Expected outcome is either 'true' | 'false' depending on the argument
  """
  @spec is_obj(tuple()) :: boolean()
  def is_obj(obj) do
    case obj do
      {{:pos, {_xPos, _yPos, _zPos}},
       {:vectors, [{_x0, _y0, _z0}, {_x1, _y1, _z1}, {_x2, _y2, _z2}]},
       {:speed, {_xVel, _yVel, _zVel}},
       {:resultingForce, {_xFor, _yFor, _zFor}}, {:type, _objType},
       {:attributes, _attributes}, {:rules, _rules}} ->
        true
      _ ->
        false
    end
  end

  @doc """
  Method parseint/1 takes a rule as a list of strings
  Expected outcome is a tuple with the correctly parsed FAPlang instruction
  """
  @spec parseint(list()) :: tuple()
  def parseint([h | t]) do
    cond do
      String.match?(h, ~r/^\s*on\s+\w+\s+do/) ->
        [_, ev | _] = String.split(h, ~r/\s+/, trim: true)
        {_rest, body} = parse_body(t, [])
        {:on, {:event, parse_ev(ev)}, body}
      String.match?(h, ~r/^\s*on\s+global\s+\w+\s+do/) ->
        [_, ev | _] = String.split(h, ~r/\s+/, trim: true)
        {_rest, body} = parse_body(t, [])
        {:on_global, {:event, parse_ev(ev)}, body}
      true -> {:undef, "Error"}
    end
  end

  @doc """
  Method parse_match/2 takes a list of matchclauses and the rest of the rule as a string
  Expected outcome is a tuple with the correctly parsed FAPlang instruction
  """
  @spec parse_match(list(), String.t()) :: tuple()
  def parse_match(list) do parse_match(list, []) end
  #def parse_match([], clauses) do {[], clauses} end
  def parse_match([h | t], clauses) do
    cond do
      String.match?(h, ~r/^\s*(\S+\s+)+->(\s*\S+\s+)*/) ->
        [condi, _body | _] = String.split(h, "->")
        {_rest, expr} = parse_expr(condi, [])
        {rest, body_eval} = parse_body(t, [])
        parse_match(rest, clauses ++ [{:matchclause, expr, body_eval}])
      String.match?(h, ~r/^\s*end/) ->
        {t, clauses}
      # Unrecognised syntax
      true ->
        {:undef, "Error match: #{h}"}
    end
  end

  @doc """
  Method parse_body/2 takes a list of strings and the rest of the rule as a string
  Expected outcome is a tuple with the correctly parsed FAPlang instruction
  """
  @spec parse_body(list(), String.t()) :: tuple()
  def parse_body([h | t], todo) do
    cond do
      String.match?(h, ~r/\s*end/) ->
        parse_body(todo, t, :end)
      true ->
        {tail, expr} = parse_expr(h, t)
        #IO.write("Body: #{inspect(expr)}\n")
        parse_body(tail, todo ++ [expr])
    end
  end

  @doc """
  Method parse_body/3 takes a string that is the body to be parsed and the rest of the rule as a string
  Expected outcome is a tuple with the correctly parsed FAPlang instruction
  """
  @spec parse_body(String.t(), String.t(), atom()) :: tuple()
  def parse_body(todo, t, :end) do
    {t, {:do, todo}}
  end

  @doc """
  Method parse_expr/2 takes an expression as a string and the rest of the rule as a string
  Expected outcome is a tuple with the correctly parsed FAPlang instruction
  """
  @spec parse_expr(String.t(), String.t()) :: tuple()
  def parse_expr(expr, following) do
    cond do
      String.match?(expr, ~r/^\s*match\s+(\S+\s+)+do/) ->
        [_, exprS | _] = String.split(expr, " ", trim: true)
        {_rest, expr} = parse_expr(exprS, following)
        {rest, matchTree} = parse_match(following)
        {rest, {:match, expr, matchTree}}
      String.match?(expr, ~r/^\s*\S+\s+\+\s+\S+/) ->
        [a, b | _] = String.split(expr, "+", trim: true)
        {_rest, aP} = parse_expr(String.trim(a), [])
        {_rest, bP} = parse_expr(String.trim(b), [])
        {following, {:add, aP, bP}}
      String.match?(expr, ~r/^\s*\S+\s+-\s+\S+/) ->
        [a, b | _] = String.split(expr, "-", trim: true)
        {_rest, aP} = parse_expr(String.trim(a), [])
        {_rest, bP} = parse_expr(String.trim(b), [])
        {following, {:sub, aP, bP}}
      String.match?(expr, ~r/^\s*\S+\s+\*\s+\S+/) ->
        [a, b | _] = String.split(expr, "*", trim: true)
        {_rest, aP} = parse_expr(String.trim(a), [])
        {_rest, bP} = parse_expr(String.trim(b), [])
        {following, {:mul, aP, bP}}
      String.match?(expr, ~r/^\s*\S+\s+\/\s+\S+/) ->
        [a, b | _] = String.split(expr, "/", trim: true)
        {_rest, aP} = parse_expr(String.trim(a), [])
        {_rest, bP} = parse_expr(String.trim(b), [])
        {following, {:div, aP, bP}}
      String.match?(expr, ~r/^\s*".*"\s*$/) ->
        [_, str | _] = String.split(expr, "\"")
        {following, {:string, str}}
      String.match?(expr, ~r/^\s*\w+\S+\:\s*\(\s*((\S+)(\s*,(\s|\S)+)*)*\s*\)/) ->
        [a, b, c | _] = String.split(expr, ":", trim: true)
        c = String.replace(c, ~r/[\(\)]/, "")
        args = String.split(c, ~r/\s*,\s*/, trim: true)
        args = parse_args(args) #retreieve complete tuple to evaluate
        {following, {:func, {String.trim(a), String.to_atom(String.trim(b)), args}}}
      String.match?(expr, ~r/^\s*([a-z]|[A-Z]|_)+\.([a-z]|[A-Z]|_)+\s+=\s+\S+/) ->
        [gobj, val | _] = String.split(expr, "=", trim: true)
        [objname, key | _] = String.split(gobj, ".", trim: true)
        {_rest, expr2} = parse_expr(val, following)
        {following, {:put, {String.to_atom(String.trim(objname)), String.trim(key)}, expr2}}
      String.match?(expr, ~r/^\s*([a-z]|[A-Z]|_)+\.([a-z]|[A-Z]|_)+/) ->
        [objname, key | _] = String.split(expr, ".", trim: true)
        {following, {:get, {String.to_atom(String.trim(objname)), String.trim(key)}}}
      String.match?(expr, ~r/^\s*[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/) ->
        {following, {:float, String.to_float(String.trim(expr))}}
      String.match?(expr, ~r/^\s*<=\s+\S+/) ->
        [_a, b | _] = String.split(expr, " ", trim: true)
        {_rest, num} = parse_expr(b, [])
        {following, {:less, num}}
      String.match?(expr, ~r/^\s*<\s+\S+/) ->
        [_a, b | _] = String.split(expr, " ", trim: true)
        {_rest, num} = parse_expr(b, [])
        {following, {:less, num}}
      String.match?(expr, ~r/^\s*>=\s+\S+/) ->
        [_a, b | _] = String.split(expr, " ", trim: true)
        {_rest, num} = parse_expr(b, [])
        {following, {:greater, num}}
      String.match?(expr, ~r/^\s*>\s+\S+/) ->
        [_a, b | _] = String.split(expr, " ", trim: true)
        {_rest, num} = parse_expr(b, [])
        {following, {:greater, num}}
      String.match?(expr, ~r/^\s*kbry/) ->
        {following, {:kbry}}
      true ->
        {following, {:undef, "Error in expr"}}
    end
  end

  @doc """
  Method parse_args/1 takes a list of arguments to parse
  Expected outcome is a list of the same arguments but in syntax so that the evaluator can evaluate it
  """
  @spec parse_args(list()) :: list()
  def parse_args([]) do [] end
  def parse_args([h|t]) do
    {_rest, expr} = parse_expr(h, [])
    [expr | parse_args(t)]
  end

  @doc """
  Method parse_ev/1 takes a string argument and matches it with the correct event
  Expected outcome is the atom of the string or undefined if the event was not recognised
  """
  @spec parse_ev(String.t()) :: atom()
  def parse_ev(ev) do
    case ev do
      "tick" -> :tick
      "collision" -> :collision
      "update" -> :update
      "remove" -> :remove
      "add" -> :add
      "connect" -> :connect
      "disconnect" -> :disconnect
      "chat" -> :chat
      _ -> :undefined
    end
  end

  @doc """
  Method parse/1 that is used for parsing a FAPlang string to a tuple-tree that the evaluator can evaluate
  Expected outcome is a correctly built tuple-tree based on the string input
  """
  @spec parse(String.t()) :: tuple()
  def parse(input) do
    parseint(String.split(input, "\n", trim: true))
  end

#-------------------------------------------------------------------------------
  #unit testing
  def test() do
    parse("""
    on tick do
      match this_attr.fireTime do
        > 1.0 ->
          this_attr.fireTime = this_attr.fireTime - 1.0
          this_attr.health = this_attr.health - 0.1
        end
         ->
          this_attr.fireTime = 0.0
        end
      end
    end
    """)
  end
#-------------------------------------------------------------------------------
end
