defmodule Routing do
  require Evallist
  use GenServer

  @moduledoc """
  Documentation for Routing.
  """

  @doc """
  Connect
  connects the client to the master server

  broadcast
  sends the data to the other nodes
  ## Examples

  #  iex> Routing.connect(127.0.0.1)
  #   :ok

  #  iex> Routing.broadcast({"command", "remove", "uuid",uuid})
  """
  @clientp 8086
  @bindp 8080
  @fromclient 8088
  @localhost {127,0,0,1}

  def start_link() do
    GenServer.start_link(__MODULE__,[])
  end

  def init(_) do
    :gen_udp.open(@fromclient)
  end

  def handle_info({:udp, _, _,_,data}, socket) do
    #broadcast(data)
    send(:command,{:cmd,List.to_string(data)})
    {:noreply, socket, :hibernate}
  end

  def listen(route_list,th_node_list,count,evallength,nodlistlen) do
    nroute_list = route_list
    nth_node_list = th_node_list
    if(length(th_node_list) < length(Node.list())+1) do
      spawn(fn -> broadcast({:th_rq,Node.self()}) end)
    end

    #if(count >= 25) do
    #  spawn(fn -> broadcast({:route_flush}) end)
    #end
#    if(count > 15) do
#      send(:fapevallist,{:get,self()})
#    end

    #if(length(Node.list()) != nodlistlen) do
    #  send(:fapevallist,{:get_route,self()})
    #end
    nodelistlen = length(Node.list())
    
    res = receive do
      {:listen,{:th_rq,node}} ->
	send({:listener,node},{:listen,{:th_ans,(evallength/System.schedulers_online()),Node.self()}})
      {:listen,{:th_ans,score,node}} ->
	nth_node_list = case (Enum.member?(nth_node_list,{score,node})) do
			  false -> nth_node_list = [{score,node}|th_node_list]
			  _ ->
			    nth_node_list
			end
	route_res = cond do
	  length(nth_node_list) == length(Node.list())+1 ->
	    route(nth_node_list)
	  true ->
	    route_list
	end
	{:th_ans_res, nth_node_list, route_res}
      {:cleanse,list} ->
	eval_cleanse(list)
	{:ok}
      {:listen,{:json, data}} ->
	to_client(data)
      {:listen,{:chat,msg}} ->
	to_client(msg)
      {:ok,list} ->
	{:nlist, length(list)}
      {:listen,{:connect}} ->
        send(:fapevallist,{:get_route,self()})
	{:ret}
      {:listen,{:evallist,list}} ->
	distrib_eval(list)
	{:redist}
      {:listen, {:route_flush}} ->
	{:route_flush}
      {:listen,{:store,data}} ->
	send(:store,data)
      {:count_change,eval_count} ->
	{:nlist,eval_count}
      {:tick} ->
	{:tick}
      {:route,:ok} -> #do not touch >:(
	{:nah}
      {:route,func} ->
	[{a,node} | t] = route_list
	Node.spawn(node, func)
	#{:ret}
	{:ret_route, t ++ [{a, node}]}
      {:get_route_list, proc} ->
        send(proc, route_list)
      b ->
	Faplogger.log(:routing, "#{inspect(b)}")
	{:ok}
    end

    case res do
      {:route_flush} ->
	listen([{(evallength/System.schedulers_online()),Node.self()}],[{(evallength/System.schedulers_online()),Node.self()}],0,evallength,nodelistlen)
      {:route_res,nlist} ->
	listen(route_list,nlist,count,evallength,nodelistlen)
      {:th_ans_res,nlist,rlist} ->
	listen(rlist,nlist,count,evallength,nodelistlen)
      {:tick} ->
	listen(route_list,th_node_list,count+1,evallength,nodelistlen)
      {:redist} ->
	listen(route_list,th_node_list,count,0,nodelistlen)
      {:ret_route, rlist} ->
	listen(rlist, th_node_list, count, evallength, nodelistlen)
      {:nlist,nlength} ->
	listen(route_list,th_node_list,count,nlength,nodelistlen)
      _ ->
	listen(route_list,th_node_list,count,evallength,nodelistlen)
    end
  end

  ##Makes the routing table used for load balancing

  def route(l) do
    Enum.sort(l,fn ({scorea,_},{scoreb,_}) -> scorea < scoreb end)
  end

  def get_route_list() do
    send(:listener, {:get_route_list, self()})
    receive do
      res -> res
    end
  end



  def start_lobby(ip) do
    :net_kernel.start([:"gamer@#{ip}"])
    Node.set_cookie(:fap)
    Process.register(spawn(fn -> listen([{0.0,Node.self()}],[{0.0,Node.self()}],0,0,length(Node.list())) end),:listener)
  end

  def start_lobby(ip, pass) do
    :net_kernel.start([:"gamer@#{ip}"])
    Node.set_cookie(String.to_atom(pass))
    Process.register(spawn(fn -> listen([{0.0,Node.self()}],[{0.0,Node.self()}],0,0,length(Node.list())) end),:listener)
  end

  def connect(ip,master_ip) do
    :net_kernel.start([:"gamer@#{ip}"])
    Node.set_cookie(:fap)
    Node.ping :"gamer@#{master_ip}"
    Process.register(spawn(fn -> listen([{0.0,Node.self()}],[{0.0,Node.self()}],0,0,length(Node.list())) end),:listener)
  end

  def connect(ip,master_ip, pass) do
    :net_kernel.start([:"gamer@#{ip}"])
    Node.set_cookie(String.to_atom(pass))
    Node.ping :"gamer@#{master_ip}"
    Process.register(spawn(fn -> listen([{0.0,Node.self()}],[{0.0,Node.self()}],0,0,length(Node.list())) end),:listener)
  end
  ##Does the load balancing and package routing
  def cluster_route(func) do
    send({:listener, Node.self()},{:route,func})
  end

  def broadcast(data) do
    broadcast(data,Node.list())
  end
  def broadcast(_,[]) do
  end
  def broadcast(data,[h|t]) do
    send({:listener,h},{:listen,data})
    broadcast(data,t)
  end

  def broadcast_n(tag, data) do
    broadcast_n(tag, data, Node.list())
  end
  def broadcast_n(_,_,[]) do :done end
  def broadcast_n(tag, data, [h|t]) do
    send({tag, h}, data)
    broadcast_n(tag, data ,t)
  end

  def client_init() do
    {:ok,sender} = :gen_udp.open(@bindp,[{:sndbuf, 32768}])
    Process.register(spawn(fn -> data_to_client(sender) end),:to_client)
  end

  def data_to_client(sender) do
    receive do
      {:client_data, data} ->
	spawn(fn -> send_data(data,sender)end)
    end
    data_to_client(sender)
  end

  #Extreme pullout method
  def disconnect() do
    send({:listener,hd(Node.list())},{:cleanse,Evallist.get()})
    send({:flogger, Node.self()}, {:close})
    System.stop(0) #Commit honorable seppuku
  end

  def eval_cleanse(evall) do
    send({:listener,Node.self()},{:listener, {:route_flush}})
    Process.sleep(30)
    distrib_eval(evall)
  end

  def distrib_eval([]) do [] end
  def distrib_eval([h|t]=_evall) do
    cluster_route(Evallist.add(h))
    distrib_eval(t)
  end

  def clean_eval([]) do [] end
  def clean_eval([{id, _}|t]) do
    Evallist.remove(id)
    clean_eval(t)
  end

  def to_client(data) do
    send(:to_client,{:client_data,data})
  end

  def send_data(data, sender) do
    payload = IO.iodata_to_binary(String.to_charlist(data))
    :ok = :gen_udp.send(sender,@localhost,@clientp, payload)
  end

  def start_routing() do
    start_link()
    client_init()
    :true
  end
end
