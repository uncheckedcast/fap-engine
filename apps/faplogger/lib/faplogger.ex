defmodule Faplogger do
  @moduledoc """
  Documentation for Faplogger.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Faplogger.hello()
      :world

  """

  def init() do
    path = Path.join([File.cwd!(), "/log_#{inspect(DateTime.to_unix(DateTime.utc_now()))}.txt"])
    res = File.open(path, [:utf8, :write])

    case res do
      {:ok, file} ->
        Process.register(spawn(fn -> handler(file) end), :flogger)
        log(:logger, "Log path: #{inspect(File.cwd!())}")
      err ->
        IO.write("[ERR] Could not open logfile: #{inspect(err)}\n")
    end
  end

  def handler(file) do
    res = receive do
      {:log, tag, msg} ->
        str = "#{DateTime.to_string(DateTime.utc_now())} [LOG] [#{Atom.to_string(tag)}] #{msg}\n"
        IO.write(str)
        IO.write(file, str)
        :log
      {:err, tag, msg} ->
        str = "#{DateTime.to_string(DateTime.utc_now())} [ERR] [#{Atom.to_string(tag)}] #{msg}\n"
        IO.write(:stderr, str)
        IO.write(file,str)
        #IO.write("Flush: #{inspect(StringIO.flush(file))}\n")
        :err
      {:close} ->
        #StringIO.flush(file)
        File.close(file)
        :close
      o -> Faplogger.err(:faplogger, "Got malformed message: #{inspect(o)}")
    end
    case res do
      :close -> :close
      _ -> handler(file)
    end
  end

  def log(tag, msg) do
    send({:flogger, Node.self()}, {:log, tag, msg})
  end

  def err(tag, msg) do
    send({:flogger, Node.self()}, {:err, tag, msg})
  end

  def close() do
    send({:flogger, Node.self()}, {:close})
  end

F
F
F
end
