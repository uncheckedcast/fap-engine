defmodule StoreTest do
  use ExUnit.Case
  doctest Store

  test "Test add_obj" do
    assert Store.add_obj(make_ref(), :Player, "Any obj struct")
  end
  
  test "Test to get whole world" do
	assert Store.get_world()
  end
  
  test "Test update_obj" do
	test_UUID = make_ref()
	Store.add_obj(test_UUID, :Player, "Any obj struct")
	assert Store.update_obj(test_UUID, "New obj struct")
  end
  
  test "Test remove_obj" do
	test_UUID = make_ref()
	Store.add_obj(test_UUID, :Player, "Any obj struct")
	assert Store.remove_obj(test_UUID)
	assert "Trying to get removed object"
	assert Store.get_obj(test_UUID)
  end
  
  test "Test get_obj" do
	test_UUID = make_ref()
	Store.add_obj(test_UUID, :Player, "Any obj struct")
	assert Store.get_obj(test_UUID)
  end
  
  test "Test get_type" do
	test_UUID = make_ref()
	Store.add_obj(test_UUID, "player", "Any obj struct")
	test_UUID2 = make_ref()
	Store.add_obj(test_UUID2, "player", "Any obj struct2")
	test_UUID3 = make_ref()
	Store.add_obj(test_UUID3, "player3", "Any obj struct3")

        assert Store.get_by_type(:Player) == {:ok, [{test_UUID, "Any obj struct"}, {test_UUID2, "Any obj struct2"}]} ||  {:ok, [{test_UUID2, "Any obj struct2"}, {test_UUID, "Any obj struct"}]}

  end

  test "Test binary to string" do
	ref = make_ref()
	assert ref
	assert List.to_string(Store.int_list_to_char_list(:erlang.binary_to_list(:erlang.term_to_binary(ref))))
  end
  
  test "Reference to string" do
	
  end
  
end
