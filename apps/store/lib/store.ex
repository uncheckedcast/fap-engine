defmodule Store do
  require Routing
  require Evallist
  #require Store.Native
  @moduledoc """
  Implements the funtionality to store objects in the world
  """
  @type obj_type :: String.t()
  @type obj :: {{:pos, {float(), float(), float()}},
         {:vectors, list()},
         {:speed, {float(), float(), float()}},
         {:resultingForce, {float(), float(), float()}}, {:type, obj_type()},
         {:attributes, any()}, {:rules, list()}}

  @spec get_world() :: list()
  def get_world() do
	case Store.Native.getAll() do
          {:ok, list} ->
            {:ok, rebuild_list(list)}
          {:error, error} -> {:error, error}
        end
  end

  def sync_world() do
    send({:store, hd(Node.list)}, {:sync_world, {:store, Node.self()}})
  end

  def sync_world(:error) do
    #IO.write("error\n")
    :error
  end

  def sync_world([{uuid, obj}|t]) do
    add_obj(uuid, extract_type(obj), obj, :nope)
    sync_world(t)
  end

  def sync_world([]) do :ok end

  @spec add_obj(String.t(), obj_type(), any()) :: tuple()
  def add_obj(uuid, type, obj) do add_obj(uuid, type, obj, :broad) end
  def add_obj(uuid, type, obj, broad) do
        case broad do
          :broad ->
            Routing.broadcast_n(:store, {:add, uuid, type, obj})
            #IO.write("Send add!\n")
            case Evaluator.Parser.is_obj(obj) do
              true ->
                Routing.cluster_route(fn -> Evallist.add({uuid, Evallist.obj_rule_parse(obj)}) end)
              false ->
                :not_obj
            end
	    #IO.write("Send load balance the object")
          _ ->
			:ok
			send({:command,Node.self()},{:store_add, uuid, obj})
        end
	##send({:command,Node.self()},{:store_add, uuid, obj})
	Store.Native.addObj(uuid, type, :erlang.term_to_binary(obj))
  end

  @spec update_obj(String.t(), any()) :: any()
  def update_obj(uuid, obj) do update_obj(uuid, obj, :broad) end
  def update_obj(uuid, obj, broad) do
	case  Store.Native.getObjType(uuid) do
	  {:ok, type} ->
            case broad do
	      :broad ->
                Routing.broadcast_n(:store, {:update, uuid, obj})
	      _ ->
		{}
	    end
	    send({:command,Node.self()},{:store_update, uuid, obj})
	    Store.Native.updateObj(uuid, type, :erlang.term_to_binary(obj))
	  _ ->
	    #add_obj(uuid, extract_type(obj), obj, :fuck)
	    {:error, "no obj with id"}
	  end
  end

  @spec remove_obj(String.t()) :: atom()
  def remove_obj(uuid) do remove_obj(uuid, :broad) end
  def remove_obj(uuid, broad) do
        case broad do
          :broad ->
            Routing.broadcast_n(:store, {:remove, uuid})
          _ -> {}
        end
        Evallist.remove(uuid)
	send({:command,Node.self()},{:store_remove, uuid})
	#IO.write("removed obj #{inspect(uuid)}\n")
	Store.Native.removeObj(uuid)
  end

  @spec get_obj(String.t()) :: any()
  def get_obj(uuid) do
	case Store.Native.getObj(uuid) do
          {:ok, binData} ->
            {:ok, :erlang.binary_to_term(binData)}
          {:error, msg} -> {:error, msg}
        end
  end

  def get_pos(obj) do
    {{:pos, pos},
     {:vectors, _vecs},
     {:speed, {_xSpeed, _ySpeed, _zSpeed}},
     {:resultingForce, {_xFor, _yFor, _zFor}}, {:type, _objType},
     {:attributes, _attributes}, {:rules, _rules}} = obj
     pos
  end

  @spec ref_to_string(reference()) :: String.t()
  def ref_to_string(ref) do
    :base64.encode(:erlang.term_to_binary(ref))
  end

  def string_to_ref(str) do
    :erlang.binary_to_term(:base64.decode(str))
  end

  @spec get_by_type(obj_type()) :: any()
  def get_by_type(type) do
    case Store.Native.getByType(type) do
      {:ok, list} ->
        {:ok, rebuild_list(list)}
      {:error, error} ->
        {:error, error}
    end
  end

  def num_types(type) do
    case get_by_type(type) do
      {:ok, list} ->
        length(list) / 1.0
      _ ->
        0.0
    end
  end

  @spec update_attributes(String.t(), any()) :: any()
  def update_attributes(uuid, attrib) do update_attributes(uuid, attrib, :broad) end
  def update_attributes(uuid, attrib, broad) do
    case get_obj(uuid) do
      {:ok, orig_obj} ->
        {{:pos, {xPos, yPos, zPos}},
         {:vectors, [{x0,y0,z0}, {x1, y1, z1}, {x2, y2, z2}]},
         {:speed, {xVel, yVel, zVel}},
         {:resultingForce, {xFor, yFor, zFor}}, {:type, objType},
         {:attributes, _attributes}, {:rules, rules}} = orig_obj

         new_obj = {{:pos, {xPos, yPos, zPos}},
         {:vectors, [{x0,y0,z0}, {x1, y1, z1}, {x2, y2, z2}]},
         {:speed, {xVel, yVel, zVel}},
         {:resultingForce, {xFor, yFor, zFor}}, {:type, objType},
         {:attributes, attrib}, {:rules, rules}}

         update_obj(uuid, new_obj, :nope)
         case broad do
          :broad ->
            Routing.broadcast_n(:store, {:update_attrib, uuid, attrib})
          _ ->
            {}
          end
          {:ok, "Updated"}
      {:error, msg} ->
        {:error, msg}
    end
  end

  def update_speed(uuid,speed) do update_speed(uuid, speed, :broad) end
  def update_speed(uuid,speed, broad) do
    case get_obj(uuid) do
      {:ok, orig_obj} ->
        {{:pos, pos},
         {:vectors, [{x0,y0,z0}, {x1, y1, z1}, {x2, y2, z2}]},
         {:speed, {_xVel, _yVel, _zVel}},
         {:resultingForce, {xFor, yFor, zFor}}, {:type, objType},
         {:attributes, attributes}, {:rules, rules}} = orig_obj

         new_obj = {{:pos, pos},
         {:vectors, [{x0,y0,z0}, {x1, y1, z1}, {x2, y2, z2}]},
         {:speed, speed},
         {:resultingForce, {xFor, yFor, zFor}}, {:type, objType},
         {:attributes, attributes}, {:rules, rules}}

         update_obj(uuid, new_obj, :nope)
         case broad do
          :broad ->
            Routing.broadcast_n(:store, {:update_speed, uuid, speed})
          _ ->
            {}
          end
          {:ok, "Updated"}
      {:error, msg} ->
        {:error, msg}
    end
  end

  def update_pos(uuid, pos) do update_pos(uuid, pos, :broad) end
  def update_pos(uuid, pos, broad) do
    case get_obj(uuid) do
      {:ok, orig_obj} ->
        {{:pos, _pos},
         {:vectors, [{x0,y0,z0}, {x1, y1, z1}, {x2, y2, z2}]},
         {:speed, {xVel, yVel, zVel}},
         {:resultingForce, {xFor, yFor, zFor}}, {:type, objType},
         {:attributes, attributes}, {:rules, rules}} = orig_obj

         new_obj = {{:pos, pos},
         {:vectors, [{x0,y0,z0}, {x1, y1, z1}, {x2, y2, z2}]},
         {:speed, {xVel, yVel, zVel}},
         {:resultingForce, {xFor, yFor, zFor}}, {:type, objType},
         {:attributes, attributes}, {:rules, rules}}

         update_obj(uuid, new_obj, :nope)
         case broad do
          :broad ->
            Routing.broadcast_n(:store, {:update_pos, uuid, pos})
          _ ->
            {}
          end
          {:ok, "Updated"}
      {:error, msg} ->
        {:error, msg}
    end
  end

  def update_vecs(uuid, vecs) do update_vecs(uuid, vecs, :broad) end
  def update_vecs(uuid, vecs, broad) do
    case get_obj(uuid) do
      {:ok, orig_obj} ->
        {{:pos, pos},
         {:vectors, [{_x0,_y0,_z0}, {_x1, _y1, _z1}, {_x2, _y2, _z2}]},
         {:speed, {xVel, yVel, zVel}},
         {:resultingForce, {xFor, yFor, zFor}}, {:type, objType},
         {:attributes, attributes}, {:rules, rules}} = orig_obj

         new_obj = {{:pos, pos},
         {:vectors, vecs},
         {:speed, {xVel, yVel, zVel}},
         {:resultingForce, {xFor, yFor, zFor}}, {:type, objType},
         {:attributes, attributes}, {:rules, rules}}

         update_obj(uuid, new_obj, :nope)
         case broad do
          :broad ->
            Routing.broadcast_n(:store, {:update_vecs, uuid, vecs})
          _ ->
            {}
          end
          {:ok, "Updated"}
      {:error, msg} ->
        {:error, msg}
    end
  end

  def update_force(uuid, force) do update_force(uuid, force, :broad) end
  def update_force(uuid, force, broad) do
    case get_obj(uuid) do
      {:ok, orig_obj} ->
        {{:pos, pos},
         {:vectors, [{x0,y0,z0}, {x1, y1, z1}, {x2, y2,z2}]},
         {:speed, {xVel, yVel, zVel}},
         {:resultingForce, {_xFor, _yFor, _zFor}}, {:type, objType},
         {:attributes, attributes}, {:rules, rules}} = orig_obj

         new_obj = {{:pos, pos},
         {:vectors, [{x0,y0,z0}, {x1, y1, z1}, {x2, y2,z2}]},
         {:speed, {xVel, yVel, zVel}},
         {:resultingForce, force}, {:type, objType},
         {:attributes, attributes}, {:rules, rules}}

         update_obj(uuid, new_obj, :nope)
         case broad do
          :broad ->
            Routing.broadcast_n(:store, {:update_force, uuid, force})
          _ ->
            {}
          end
          {:ok, "Updated"}
      {:error, msg} ->
        {:error, msg}
    end
  end

  @spec init() :: pid()
  def init() do
    Process.register(spawn(fn -> handler() end), :store)
  end

  def handler() do
    receive do
      {:update, id, obj} ->
        update_obj(id, obj, :nope)
      {:add, id, type, obj} ->
        add_obj(id, type, obj, :nope)
        #IO.write("Got add!\n")
      {:remove, id} ->
        remove_obj(id, :nope)
      {:update_attrib, uuid, attrib} ->
        update_attributes(uuid, attrib, :nope)
        #IO.write("Got attrib! #{inspect(attrib)}\ni")
      {:update_pos, uuid, pos} ->
        update_pos(uuid, pos, :nope)
      {:update_speed,uuid,speed} ->
	update_speed(uuid,speed,:nope)
      {:update_vecs, uuid, vecs} ->
        update_vecs(uuid, vecs, :nope)
      {:sync_world, proc} ->
        {:ok, world} = get_world()
        send(proc, {:world, world})
      {:world, world} ->
	sync_world(world)
      other ->
        Faplogger.err(:store, "Got malformed object #{inspect(other)}")
    end
    handler()
  end

  def rebuild_list(orig) do
    rebuild_list(orig, [])
  end

  def rebuild_list([], newl) do
    newl
  end

  def rebuild_list([h | t], newl) do
    {uuid, binData} = h
    #IO.write("#{inspect(uuid)}: #{inspect(binData)}")
    rebuild_list(t, [{uuid, :erlang.binary_to_term(binData)} | newl])
  end

  @spec extract_type(obj()) :: obj_type()
  def extract_type(obj) do
        {{:pos, {_xPos, _yPos, _zPos}},
         {:vectors, [{_x0,_y0,_z0}, {_x1, _y1, _z1}, {_x2, _y2, _z2}]},
         {:speed, {_xVel, _yVel, _zVel}},
         {:resultingForce, {_xFor, _yFor, _zFor}}, {:type, objType},
         {:attributes, _attributes}, {:rules, _rules}} = obj
         objType
  end
end
