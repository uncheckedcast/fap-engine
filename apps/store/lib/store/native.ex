defmodule Store.Native do

  use Rustler, otp_app: :store, crate: :store
  
  def addObj(_, _ ,_), do: error()
  def updateObj(_, _, _), do: error()
  def removeObj(_), do: error()
  def getObj(_), do: error()
  def getAll(), do: error()
  def getByType(_), do: error()  
  def getObjType(_), do: error()
  def getInfo(), do: error()
  def clear(), do: error()

  defp error, do: :erlang.nif_error(:nif_not_loaded)
  
 end
