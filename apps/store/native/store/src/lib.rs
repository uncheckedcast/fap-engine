#[macro_use] extern crate rustler;
/*#[macro_use]*/ extern crate rustler_codegen;
#[macro_use] extern crate lazy_static;

use rustler::{Env, Term, NifResult, Encoder, Binary, OwnedBinary};
//use rustler::types::{tuple};
//use rustler::error::Error;

use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use std::vec::Vec;
use std::string::String;
use std::collections::hash_map::Entry;
//use std::cell::RefCell;
//use std::borrow::BorrowMut;

mod atoms {
    rustler_atoms! {
        atom ok;
        atom error;
        //atom __true__ = "true";
        //atom __false__ = "false";
    }
}

rustler_export_nifs! {
    "Elixir.Store.Native",
    [
        ("addObj", 3, add),
        ("getObj", 1, get),
        ("removeObj", 1, remove),
        ("updateObj", 3, update),
        ("getByType", 1, get_type), 
        ("getAll", 0, get_all),
        ("getObjType", 1, get_obj_type),
        ("getInfo", 0, get_info),
        ("clear", 0, clear)
    ],
    Some(on_load)
}

fn on_load(_env: Env, _info: Term) -> bool {
    true
}

/*thread_local!{
    static STORE: RefCell<HashMap<String, ItemWrapper>> = RefCell::new({let m = HashMap::new(); m});
}*/
lazy_static!{
    static ref STORE: Arc<Mutex<HashMap<String, ItemWrapper>>> = Arc::new({Mutex::new(HashMap::new())});
}

struct ItemWrapper {
    itype: String,
    item: OwnedBinary,
}

fn extend_life(old: Binary) -> OwnedBinary {
    let mut new_bin: OwnedBinary = OwnedBinary::new(old.as_slice().len()).unwrap();
    new_bin.as_mut_slice().copy_from_slice(old.as_slice());
    return new_bin;
}

fn get_info<'a>(env: Env<'a>, args: &[Term<'a>]) -> NifResult<Term<'a>> {
    let mut map = STORE.lock().unwrap();
    let length = map.len();
    let mut byte_size = 0;
    for val in map.values() {
        byte_size += std::mem::size_of_val(&val.itype);
        byte_size += std::mem::size_of_val(val.item.as_slice());
    }
    let capacity = map.capacity();
    Ok((atoms::ok(), byte_size, length, capacity).encode(env))
}

fn clear<'a>(env: Env<'a>, args: &[Term<'a>]) -> NifResult<Term<'a>> {
    let mut map = STORE.lock().unwrap();
    for (k, v) in map.drain().take(1) {
        std::mem::drop(k);
        std::mem::drop(v);
    }
    map.shrink_to_fit();
    Ok(atoms::ok().encode(env))
}

fn add<'a>(env: Env<'a>, args: &[Term<'a>]) -> NifResult<Term<'a>> {
    //let res = STORE.with(|store| {
        let id_str: String = args[0].decode().unwrap_or_default();
        let _id: Vec<u8> = id_str.as_str().as_bytes().to_vec();
        
        let itype: String = args[1].decode().unwrap_or_default();
        let item: Binary; 
        if args[2].decode::<Binary>().is_ok() {
            item = args[2].decode().ok().unwrap();
        } else {
            return Ok((atoms::error(), "Error parsing object").encode(env))
        }

        let mut map = STORE.lock().unwrap();
        
        match map.entry(id_str) {
            Entry::Occupied(_) => {
                Ok((atoms::error(), "Key already present").encode(env))
            },
            Entry::Vacant(val) => { 
                val.insert(ItemWrapper{itype: itype, item: extend_life(item)});
                Ok((atoms::ok()).encode(env))
            }
        }

    //});
    //Ok(res)
}


fn get<'a>(env: Env<'a>, args: &[Term<'a>]) -> NifResult<Term<'a>> {
    //let res = STORE.with(|store| {
        let id_str: String = args[0].decode().unwrap_or_default();
        let _id: Vec<u8> = id_str.as_str().as_bytes().to_vec();

        let mut map = STORE.lock().unwrap();

        match map.entry(id_str) {
            Entry::Occupied(iwrap) => {
                let iwrapmut: &ItemWrapper = &*iwrap.into_mut();
                let mut obin: OwnedBinary = OwnedBinary::new(iwrapmut.item.len()).unwrap();
                obin.as_mut_slice().copy_from_slice(&iwrapmut.item);
                let bin: Binary = Binary::from_owned(obin, env);

                Ok((atoms::ok(), bin).encode(env))
            },
            Entry::Vacant(_) => {
                Ok((atoms::error(), "No object with id").encode(env))
            }
        }
    //});
    //Ok(res)
}

fn get_obj_type<'a>(env: Env<'a>, args: &[Term<'a>]) -> NifResult<Term<'a>> {
    //let res = STORE.with(|store| {
        let id_str: String = args[0].decode().unwrap_or_default();
        let _id: Vec<u8> = id_str.as_str().as_bytes().to_vec();

        let mut map = STORE.lock().unwrap();

        match map.entry(id_str) {
            Entry::Occupied(iwrap) => {
                let iwrapmut: &ItemWrapper = &*iwrap.into_mut();
                /*let mut obin: OwnedBinary = OwnedBinary::new(iwrapmut.item.len()).unwrap();
                obin.as_mut_slice().copy_from_slice(&iwrapmut.item);
                let bin: Binary = Binary::from_owned(obin, env);*/

                Ok((atoms::ok(), &iwrapmut.itype).encode(env))
            },
            Entry::Vacant(_) => {
                Ok((atoms::error(), "No object with id").encode(env))
            }
        }
    //});
    //Ok(res)
}

fn remove<'a>(env: Env<'a>, args: &[Term<'a>]) -> NifResult<Term<'a>> {
    //let res = STORE.with(|store| {
        let id_str: String = args[0].decode().unwrap_or_default();
        let _id: Vec<u8> = id_str.as_str().as_bytes().to_vec();
        
        let mut map = STORE.lock().unwrap();
        let res = match map.entry(id_str) {
            Entry::Occupied(val) => {
                val.remove_entry();
                Ok((atoms::ok()).encode(env))
            },
            Entry::Vacant(_) => {
                Ok((atoms::error(), "No item with id").encode(env))
            }
        };
        map.shrink_to_fit();
        res
    //});
    //Ok(res)
    //Ok((atoms::error(), "Not implemented").encode(env))
}

fn update<'a>(env: Env<'a>, args: &[Term<'a>]) -> NifResult<Term<'a>> {
    //let res = STORE.with(|store| {
        let id_str: String = args[0].decode().unwrap_or_default();
        let _id: Vec<u8> = id_str.as_str().as_bytes().to_vec();
        
        let itype: String = args[1].decode().unwrap_or_default();
        
        let item: Binary; 
        if args[2].decode::<Binary>().is_ok() {
            item = args[2].decode().ok().unwrap();
        } else {
            return Ok((atoms::error(), "Error parsing object").encode(env))
        }
        
        let mut map = STORE.lock().unwrap();

        match map.entry(id_str) {
            Entry::Occupied(mut val) => {
                let nitem = ItemWrapper{itype: itype, item: extend_life(item)};
                *val.get_mut() = nitem;
                Ok((atoms::ok()).encode(env))
            },
            Entry::Vacant(_) => {
                Ok((atoms::error(), "No such id").encode(env))
            }
        }
    //});
    //Ok(res)
    //Ok((atoms::error(), "Not implemented").encode(env))
}

fn get_type<'a>(env: Env<'a>, args: &[Term<'a>]) -> NifResult<Term<'a>> {
    //let res = STORE.with(|store| {
        let typ: String = args[0].decode().unwrap_or_default();

        let map = STORE.lock().unwrap();
        let mut tuples = Term::list_new_empty(env);

        for (key, val) in map.iter() {
            //tuples.push((key, val));
            let iwrapmut: &ItemWrapper = &*val;
            if typ == iwrapmut.itype {
                let mut obin: OwnedBinary = OwnedBinary::new(iwrapmut.item.len()).unwrap();
                obin.as_mut_slice().copy_from_slice(&iwrapmut.item);
                let bin: Binary = Binary::from_owned(obin, env);
                tuples = tuples.list_prepend((key, bin).encode(env));
            }
        }
        Ok((atoms::ok(), tuples).encode(env))
    //});
    
    //Ok(res)
    //Ok((atoms::error(), "Not implemented").encode(env))
}

fn get_all<'a>(env: Env<'a>, _args: &[Term<'a>]) -> NifResult<Term<'a>> {
    //let res = STORE.with(|store| {
        let map = STORE.lock().unwrap();
        let mut tuples = Term::list_new_empty(env);

        for (key, val) in map.iter() {
            //tuples.push((key, val));
            let iwrapmut: &ItemWrapper = &*val;
            let mut obin: OwnedBinary = OwnedBinary::new(iwrapmut.item.len()).unwrap();
            obin.as_mut_slice().copy_from_slice(&iwrapmut.item);
            let bin: Binary = Binary::from_owned(obin, env);
            tuples = tuples.list_prepend((key, bin).encode(env));
        }
        Ok((atoms::ok(), tuples).encode(env))
    //});
    //Ok(res)
    //Ok((atoms::error(), "Not implemented").encode(env))
}
