defmodule Init do
  require Commands
  use Application
  @moduledoc """
  Documentation for Init.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Init.hello()
      :world

  """
  def start(_type,_args) do
    {:ok,spawn(fn -> Commands.init() end)}
  end
  
  def main(_args) do
    Commands.init()
  end
end
