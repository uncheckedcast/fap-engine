defmodule Scheduler do

  @doc """
  Method init/0 initializes the scheduler
  Expected outcome is the atom 'true'
  """
  @spec init() :: atom()
  def init() do
    spawn(fn -> schedule(0) end)
    :true
  end

  @doc """
  Method schedule/1 takes float as argument
  Expected outcome is anything, method spawns itself before returning
  """
  @spec schedule(number()) :: any()
  def schedule(delta_time) do
    t = DateTime.utc_now()
    Process.sleep(15)
    eval_objs = get_eval_objs(Evallist.get())
    eval_list(eval_objs)
    eval_obj_list(eval_objs,delta_time)
    send({:listener,Node.self()},{:tick})
    diff = DateTime.diff(DateTime.utc_now(), t, :nanosecond) / 1000000000.0
    spawn(fn -> schedule(diff) end)
  end

  @doc """
  Method eval_list/1 takes a list of rules to evaluate
  Expected outcome is either a list or an atom if the evaluation failed
  """
  @spec eval_list(list()) :: atom()
  def eval_list([]) do [] end
  def eval_list([{id, obj, rules}= _h | t] = _eval_objs) do
    case rules do
      [] ->
        []
      _ ->
      env = Evaluator.Parser.parse_env([{:this, obj, id}, {:world, %{"num_types" => Store.num_types("PlayerObject")}, ""}])
      #Faplogger.log(:scheduler, "environment to evaluate - #{inspect(env)}")
      eval_res = Evaluator.eval_rules(rules, env, :tick)
      case eval_res do
        :failure ->
          Faplogger.err(:scheduler, "Failed to evaluate tick - #{inspect(obj)}")
          :fail
        l ->
          list = Evaluator.Parser.deparse_env(l)
          {:this, gobj2, idg2} = List.keyfind(list, :this, 0, {:this, {}, ""})
          Store.update_obj(idg2, gobj2)
        end
    end
    eval_list(t)
  end

  @doc """
  Method eval_obj_list/2 takes a list of objects and a float delta-time
  Expected outcome is a list or an atom
  """
  @spec eval_obj_list(list(), float()) :: atom()
  def eval_obj_list([], _) do [] end
  def eval_obj_list([h | t] = _eval_list, delta_time) do
    case Store.get_world() do
      {:ok, []} ->
        :empty
      {:ok, world} ->
        eval_speed_force(h, world, delta_time, [])
      {:error, err} ->
        Faplogger.err(:scheduler, "From get_world #{err}")
    end
    eval_obj_list(t, delta_time)
  end

  @doc """
  Method get_eval_objs/1 initializes get_eval_objs/2 with accumulator
  Expected outcome is a list of objects
  """
  @spec get_eval_objs(list()) :: list()
  def get_eval_objs(eval_list) do get_eval_objs(eval_list, []) end

  @doc """
  Method get_eval_objs/2 takes a list of evaluations and a list of objects
  Expected outcome is a list of objects
  """
  @spec get_eval_objs(list(), list()) :: list()
  def get_eval_objs([], objs) do objs end
  def get_eval_objs([{id, rules} | t] = _eval_list, objs) do
    case Store.get_obj(id) do
      {:ok, obj} ->
        case Evaluator.Parser.is_obj(obj) do
          true ->
            get_eval_objs(t, [{id, obj, rules} | objs])
          false ->
            get_eval_objs(t, objs)
        end
      _ ->
        get_eval_objs(t, objs)
    end
  end

  @doc """
  Method eval_speed_force/4 takes a tuple, a list and a float as arguments... and an other list. For the normal forces.
  Expected outcome is a boolean, either 'true' | 'false'
  """
  @spec eval_speed_force(tuple(), list(), float(), list()) :: boolean()
  def eval_speed_force({id, obj, rules} = _h, world, delta_time, coll) do
    {{:pos, {xPos, yPos, zPos}},
     {:vectors, [{x01,y01,z01}, {x11, y11, z11}, {x21, y21, z21}]},
     {:speed, {xSpeed, ySpeed, zSpeed}},
     {:resultingForce, {xResFor, yResFor, zResFor}}, {:type, objType1},
     {:attributes, attributes}, {:rules, orules}} = obj

    {speedx, speedy, speedz} = NMath.apply_force(obj, delta_time, Map.get(attributes, "collision", []))
    if (abs(speedx)+abs(speedy)+abs(speedz) > 0) do
      new_obj = {{:pos, new_pos = {xPos+(speedx*delta_time), yPos+(speedy*delta_time), zPos+(speedz*delta_time)}},
		 {:vectors, [{x01,y01,z01}, {x11, y11, z11}, {x21, y21, z21}]},
		 {:speed, new_speed = {speedx, speedy, speedz}},
		 {:resultingForce, {xResFor, yResFor, zResFor}}, {:type, objType1},
		 {:attributes, attributes}, {:rules, orules}}

    new_obj = {{:pos, new_pos = {xPos+(speedx*delta_time), yPos+(speedy*delta_time), zPos+(speedz*delta_time)}},
     {:vectors, [{x01,y01,z01}, {x11, y11, z11}, {x21, y21, z21}]},
     {:speed, new_speed = {speedx, speedy, speedz}},
     {:resultingForce, {xResFor, yResFor, zResFor}}, {:type, objType1},
     {:attributes, attributes}, {:rules, orules}}
    if (abs(speedx)+abs(speedy)+abs(speedz) > 0) do
      w = List.keydelete(world, id, 0)
      coll = collision_check({id, new_obj, rules},w)
      if Map.has_key?(attributes, "collision") do
	attr = Map.put(attributes, "collision", coll)
	Store.update_attributes(id, attr)
      end
      if length(coll) == 0 do
        Store.update_pos(id,new_pos)
        Store.update_speed(id,new_speed)
      end
    end    
    end
  end

  @doc """
  Method collision_check/2 takes a tuple with an object and a list with all of the other objects in the world
  Expected outcome is an atom
  """
  @spec collision_check(tuple(), list()) :: atom()
  def collision_check(this, list) do collision_check(this, list, []) end
  def collision_check(_, [], coll) do coll end
  def collision_check({id, obj, rules} = this_obj, [{other_id, other_obj} | t], coll) do
    col = case NMath.check_collision(obj, other_obj) do
	    {:ok,true} ->
	      # Legacy code
	      # {_, correct_y, _} = NMath.correct_positions(obj, other_obj)
	      # {pos_x, _, pos_z} = Store.get_pos(id)
	      Store.update_pos(id, NMath.correct_positions(obj, other_obj))
              eval_collision({id, obj}, {other_id, other_obj}, rules)
	      [other_id | coll]
	    {:ok, false} ->
              coll
	  end
    collision_check(this_obj, t, col)
  end

  @doc """
  Method eval_collision/3 takes a two tuples as objects to be evaluated and a list of rules
  Expected outcome is an atom
  """
  @spec eval_collision(tuple(), tuple(), list()) :: atom()
  def eval_collision({thisId, thisObj}, {otherId, otherObj}, rules) do
      env = Evaluator.Parser.parse_env([{:this, thisObj, thisId}, {:other, otherObj, otherId}])
      eval_res = Evaluator.eval_rules(rules, env, :collision)
      case eval_res do
        :failure ->
          :fail
        l ->
          list = Evaluator.Parser.deparse_env(l)
          case List.keyfind(list, :this, 0, :error) do
            :error ->
              Faplogger.err(:keyfind_this, "Could not locate this in list - #{inspect(list)}")
            {:this, gobj2, idg2} ->
              Store.update_obj(idg2, gobj2)
          end
          case List.keyfind(list, :other, 0, :error) do
            :error ->
              Faplogger.err(:keyfind_other, "Could not locate other in list - #{inspect(list)}")
            {:other, oobj2, ido2} ->
              Store.update_obj(ido2, oobj2)
          end
      end
  end
end
