defmodule SchedulerTest do
  use ExUnit.Case
  doctest Scheduler

  test "Init the scheduler process" do
    assert is_pid(Scheduler.init())
  end
  
  test "Do one run of scheduler" do
    assert Scheduler.schedule()
  end
  
  test "Do one run of movement" do
	Scheduler.apply_speed()
  end
  
  test "Update object with new speed" do
	obj_list = [{"ID", {{:pos, {4, 2, 0}},
    {:vectors, []},
    {:speed, {1, 1, 1}},
    {:resultingForce, {0, 0, 0}}, {:type, "type"},
    {:attributes, {}}, {:rules, []}}} | [] ]
	assert Scheduler.update_speed_objects(obj_list) 
  end
  
  test "Apply speed storde in an object to that object" do
  obj = {{:pos, {4, 2, 0}},
    {:vectors, []},
    {:speed, {1, 1, 1}},
    {:resultingForce, {0, 0, 0}}, {:type, "type"},
    {:attributes, {}}, {:rules, []}}
	newObj = Scheduler.apply_speed_object(obj)
	
	{{:pos, {xPos, yPos, zPos}},
    {:vectors, posVectors},
    {:speed, {xSpeed, ySpeed, zSpeed}},
    {:resultingForce, {xFor, yFor, zFor}}, {:type, objType},
    {:attributes, attributes}, {:rules, rules}} = newObj
	
	assert xPos = xPos + xSpeed
	assert yPos = yPos + ySpeed
	assert zPos = zPos + zSpeed
  end
  
end
