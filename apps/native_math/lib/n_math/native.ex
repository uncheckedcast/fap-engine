defmodule NMath.Native.Vec3 do
  defstruct [
      x: 0,
      y: 0,
      z: 0
  ]
end

defmodule NMath.Native do

  use Rustler, otp_app: :native_math, crate: :nmath

  def corners(_,_,_), do: error()
  def collision_detection(_,_,_,_), do: error()
  def print_elixir_data(_), do: error()
  def calcNewPosRes(_,_,_,_), do: error()
  def addForce(_,_), do: error()
  def applyForce(_,_,_,_), do: error()
  def checkCollision(_,_,_,_,_,_), do: error()
  def calculate_normal(_,_,_), do: error()
  def calculate_normal_force(_,_), do: error()
  def get_surface(_,_), do: error()
  def get_min(_,_,_), do: error()
  def get_max(_,_,_), do: error()
  def correct_pos(_,_,_,_,_,_), do: error()

  defp error, do: :erlang.nif_error(:nif_not_loaded)
 end
