defmodule NMath do
  @moduledoc """
  Calculates forces and collisions.
  """

  @xneg 0
  @yneg 1
  @zneg 2
  @xpos 3
  @ypos 4
  @zpos 5

  @spec check_collision(tuple(), tuple()) :: atom()
  def check_collision(obj1, obj2) do
    #retrieves the object parameters
    {{:pos, {xPos1, yPos1, zPos1}},
     {:vectors, [{_x01,_y01,_z01}, {x11, y11, z11}, {x21, y21, z21}]},
     {:speed, {_xVel, _yVel, _zVel}},
     {:resultingForce, {_xFor1, _yFor1, _zFor1}}, {:type, _objType1},
     {:attributes, _attributes1}, {:rules, _rules1}} = obj1

    {{:pos, {xPos2, yPos2, zPos2}},
     {:vectors, [{_x02,_y02,_z02}, {x12, y12, z12}, {x22, y22, z22}]},
     {:speed, {_xVel, _yVel, _zVel}},
     {:resultingForce, {_xFor1, _yFor1, _zFor1}}, {:type, _objType1},
     {:attributes, _attributes1}, {:rules, _rules1}} = obj2

    #Checks for collisions
    NMath.Native.checkCollision(
      %NMath.Native.Vec3{x: xPos1, y: yPos1, z: zPos1 },
      %NMath.Native.Vec3{x: x11, y: y11, z: z11 },
      %NMath.Native.Vec3{x: x21, y: y21, z: z21 },
      %NMath.Native.Vec3{x: xPos2, y: yPos2, z: zPos2 },
      %NMath.Native.Vec3{x: x12, y: y12, z: z12 },
      %NMath.Native.Vec3{x: x22, y: y22, z: z22 }
    )
  end

  def correct_positions(mov_obj, rigid_obj) do
    #retrieves the object parameters
    {{:pos, {xPos1, yPos1, zPos1}},
     {:vectors, [{_x01,_y01,_z01}, {x11, y11, z11}, {x21, y21, z21}]},
     {:speed, {_xVel, _yVel, _zVel}},
     {:resultingForce, {_xFor1, _yFor1, _zFor1}}, {:type, _objType1},
     {:attributes, _attributes1}, {:rules, _rules1}} = mov_obj

    {{:pos, {xPos2, yPos2, zPos2}},
     {:vectors, [{_x02,_y02,_z02}, {x12, y12, z12}, {x22, y22, z22}]},
     {:speed, {_xVel, _yVel, _zVel}},
     {:resultingForce, {_xFor1, _yFor1, _zFor1}}, {:type, _objType1},
     {:attributes, _attributes1}, {:rules, _rules1}} = rigid_obj

    #Checks for collisions
    new_mov_obj_pos = NMath.Native.correct_pos(
      %NMath.Native.Vec3{x: xPos1, y: yPos1, z: zPos1 },
      %NMath.Native.Vec3{x: x11, y: y11, z: z11 },
      %NMath.Native.Vec3{x: x21, y: y21, z: z21 },
      %NMath.Native.Vec3{x: xPos2, y: yPos2, z: zPos2 },
      %NMath.Native.Vec3{x: x12, y: y12, z: z12 },
      %NMath.Native.Vec3{x: x22, y: y22, z: z22 }
    )
    {Map.get(new_mov_obj_pos, :x), Map.get(new_mov_obj_pos, :y), Map.get(new_mov_obj_pos, :z)}
  end

  @doc """
  Adds force to (with vector addition) resulting force in object and returns the new resulting force
  """
  @spec add_force(tuple(), list) :: tuple()
  def add_force(obj, []) do
    #retrieves the object parameters
    {{:pos, {_xPos, _yPos, _zPos}},
     {:vectors, [{_x01,_y01,_z01}, {_x11, _y11, _z11}, {_x21, _y21, _z21}]},
     {:speed, {_xVel, _yVel, _zVel}},
     {:resultingForce, {xResFor, yResFor, zResFor}}, {:type, _objType1},
     {:attributes, _attributes1}, {:rules, _rules1}} = obj
    {xResFor, yResFor, zResFor}
  end
  def add_force(obj, force_list) do
    #retrieves the object parameters
    #Faplogger.log(:NMATH_SANDOR_DALIG_KAD, "Det här är objektet #{inspect(obj)}")
    {{:pos, {_xPos, _yPos, _zPos}},
     {:vectors, [{_x01,_y01,_z01}, {_x11, _y11, _z11}, {_x21, _y21, _z21}]},
     {:speed, {_xVel, _yVel, _zVel}},
     {:resultingForce, {xResFor, yResFor, zResFor}}, {:type, _objType1},
     {:attributes, _attributes1}, {:rules, _rules1}} = obj
    newResForce = NMath.Native.addForce(%NMath.Native.Vec3{x: xResFor, y: yResFor, z: zResFor} , make_vec3_list(force_list))
    {Map.get(newResForce, :x), Map.get(newResForce, :y), Map.get(newResForce, :z)}
  end

  defp vector_addition(og_force, force_list) do
    {xResFor, yResFor, zResFor} = og_force
    new_force = NMath.Native.addForce(%NMath.Native.Vec3{x: xResFor, y: yResFor, z: zResFor} , make_vec3_list(force_list))
    {Map.get(new_force, :x), Map.get(new_force, :y), Map.get(new_force, :z)}
  end
  
  def calc_res_force([], resFor) do
    resFor
  end
  def calc_res_force([other_id | rest_ids], resFor) do
    case Store.get_obj(other_id) do
      {:error, _} ->  calc_res_force(rest_ids, resFor)
      obj -> {:ok, {{:pos, {_xPos, _yPos, _zPos}},
		   {:vectors, [first_vec = {x01,y01,z01}, {_x11, _y11, _z11}, {_x21, _y21, _z21}]},
		    {:speed, {_xSpeed, _ySpeed, _zSpeed}},
		   {:resultingForce, {_xResFor, _yResFor, _zResFor}}, {:type, _objType1},
		   {:attributes, _attributes}, {:rules, _rules}}} = obj
	{:ok, obj} = obj
	
	#Faplogger.log(:NMATH_SANDOR_DALIG_KAD, "Det här är firt vec #{inspect(first_vec)}")
	surface = get_surface(first_vec, @ypos)
	#Faplogger.log(:NMATH_SANDOR_DALIG_KAD, "Det här är surface #{inspect(surface)}")
	normal_force = get_normal_force(resFor, surface)
	#Faplogger.log(:NMATH_SANDOR_DALIG_KAD, "Det här är normal-kraften #{inspect(normal_force)}")
	calc_res_forces(rest_ids, vector_addition(resFor, [normal_force]), resFor)
    end
  end
  def calc_res_forces([], new_resFor, og_resFor) do
    new_resFor
  end
   def calc_res_forces([other_id | rest_ids], new_resFor, og_resFor) do
    case Store.get_obj(other_id) do
      {:error, _} ->  calc_res_forces(rest_ids, new_resFor, og_resFor)
      obj -> {:ok, {{:pos, {_xPos, _yPos, _zPos}},
		   {:vectors, [first_vec = {x01,y01,z01}, {_x11, _y11, _z11}, {_x21, _y21, _z21}]},
		    {:speed, {_xSpeed, _ySpeed, _zSpeed}},
		   {:resultingForce, {_xResFor, _yResFor, _zResFor}}, {:type, _objType1},
		   {:attributes, _attributes}, {:rules, _rules}}} = obj
	{:ok, obj} = obj
	
	#Faplogger.log(:NMATH_SANDOR_DALIG_KAD, "Det här är firt vec #{inspect(first_vec)}")
	surface = get_surface(first_vec, @ypos)
	#Faplogger.log(:NMATH_SANDOR_DALIG_KAD, "Det här är surface #{inspect(surface)}")
	normal_force = get_normal_force(og_resFor, surface)
	#Faplogger.log(:NMATH_SANDOR_DALIG_KAD, "Det här är normal-kraften #{inspect(normal_force)}")
	calc_res_forces(rest_ids, vector_addition(new_resFor, [normal_force]), og_resFor )
    end
  end
  
   def apply_force(obj, delta_time, list) do
     #retrieves the object parameters
     {{:pos, {xPos, yPos, zPos}},
      {:vectors, [{x01,y01,z01}, {x11, y11, z11}, {x21, y21, z21}]},
      {:speed, {xSpeed, ySpeed, zSpeed}},
      {:resultingForce, {xResFor, yResFor, zResFor}}, {:type, objType1},
      {:attributes, attributes}, {:rules, rules}} = obj
     {xPos, yPos, zPos} = max_get(obj)
     cons_list = get_constant_force()
     {xCRes, yCRes, zCRes} = add_force(obj, cons_list)
     obj = {{:pos, {xPos, yPos, zPos}},
	    {:vectors, [{x01,y01,z01}, {x11, y11, z11}, {x21, y21, z21}]},
	    {:speed, {xSpeed, ySpeed, zSpeed}},
	    {:resultingForce, {xCRes, yCRes, zCRes}}, {:type, objType1},
	    {:attributes, attributes}, {:rules, rules}}
     #cRes = {xCRes, yCRes, zCRes}
     #Faplogger.log(:NMATH_SANDOR_DALIG_KAD, "Det här är restultantkraften efter constant forces #{inspect(cRes+)}")
     #Faplogger.log(:INNAN_COLLISION_HANMTAS, "")
     newResForce = case Map.get(attributes, "collision", nil) do
		     nil -> {xCRes, yCRes, zCRes}
		     collision_ids ->  calc_res_force(collision_ids, {xCRes, yCRes, zCRes})
		   end
     #Faplogger.log(:NMATH_SANDOR_DALIG_KAD, "Det här är newResForce #{inspect(newResForce)}")
     {xNewRes, yNewRes, zNewRes} = newResForce
     case Map.get(attributes, "mass", 0.0) do
       0.0 -> {xSpeed, ySpeed, zSpeed}
       mass ->  newSpeedVec3  = NMath.Native.applyForce(
       %NMath.Native.Vec3{x: xSpeed, y: ySpeed, z: zSpeed},
       %NMath.Native.Vec3{x: xNewRes, y: yNewRes, z: zNewRes},
       mass/1.0, delta_time)
	 #Faplogger.log(:NMATH_SANDOR_DALIG_KAD, "Det här är newSpeed #{inspect(newSpeedVec3)}")
	 {Map.get(newSpeedVec3, :x), Map.get(newSpeedVec3, :y), Map.get(newSpeedVec3, :z)}
     end
   end

   def min_get(obj) do
     {{:pos, {xPos1, yPos1, zPos1}},
      {:vectors, [{_x01,_y01,_z01}, {x11, y11, z11}, {x21, y21, z21}]},
      {:speed, {_xVel, _yVel, _zVel}},
      {:resultingForce, {_xFor1, _yFor1, _zFor1}}, {:type, _objType1},
      {:attributes, _attributes1}, {:rules, _rules1}} = obj

     min_vec3 = NMath.Native.get_min(
       %NMath.Native.Vec3{x: xPos1, y: yPos1, z: zPos1 },
       %NMath.Native.Vec3{x: x11, y: y11, z: z11 },
       %NMath.Native.Vec3{x: x21, y: y21, z: z21 }
     )
     {Map.get(min_vec3, :x), Map.get(min_vec3, :y), Map.get(min_vec3, :z)}
   end

   def max_get(obj) do
     {{:pos, {xPos1, yPos1, zPos1}},
      {:vectors, [{_x01,_y01,_z01}, {x11, y11, z11}, {x21, y21, z21}]},
      {:speed, {_xVel, _yVel, _zVel}},
      {:resultingForce, {_xFor1, _yFor1, _zFor1}}, {:type, _objType1},
      {:attributes, _attributes1}, {:rules, _rules1}} = obj

     min_vec3 = NMath.Native.get_max(
       %NMath.Native.Vec3{x: xPos1, y: yPos1, z: zPos1 },
       %NMath.Native.Vec3{x: x11, y: y11, z: z11 },
       %NMath.Native.Vec3{x: x21, y: y21, z: z21 }
     )
     {Map.get(min_vec3, :x), Map.get(min_vec3, :y), Map.get(min_vec3, :z)}
   end
   
  def calculate_normal(p1, p2, p3) do
    {:ok, n} = NMath.Native.calculate_normal(make_vec3(p1), make_vec3(p2), make_vec3(p3))
    from_vec3(n)
  end

  def get_normal_force(force, {p1, p2, p3} = _plane) do
    force = make_vec3(force)
    #{{:pos, {_xPos, _yPos, _zPos}},
    # {:vectors, [{x01,y01,z01}, {x11, y11, z11}, {x21, y21, z21}]},
    # {:speed, {_xSpeed, _ySpeed, _zSpeed}},
    # {:resultingForce, {_xResFor, _yResFor, _zResFor}}, {:type, _objType1},
    # {:attributes, _attributes}, {:rules, _rules}} = other
    normal = calculate_normal(p1, p2, p3)
    {:ok, nforce} = NMath.Native.calculate_normal_force(force, make_vec3(normal))
    from_vec3(nforce)
  end

  def get_surface(half, side) do
    {:ok, l} = NMath.Native.get_surface(make_vec3(half), side)
    [p1, p2, p3] = from_vec3_list(l)
    {p1, p2, p3}
  end

  def test_surface(half, side) do
    {:ok, l} = NMath.Native.get_surface(half, side)
    [p1, p2, p3] = from_vec3_list(l)
    calculate_normal(p1, p2, p3)
  end

  def test_surfaces() do
    half = make_vec3({1, 1, 1})
    Faplogger.log(:nmath_test, "Surface 0 #{inspect(test_surface(half, 0))}")
    Faplogger.log(:nmath_test, "Surface 1 #{inspect(test_surface(half, 1))}")
    Faplogger.log(:nmath_test, "Surface 2 #{inspect(test_surface(half, 2))}")
    Faplogger.log(:nmath_test, "Surface 3 #{inspect(test_surface(half, 3))}")
    Faplogger.log(:nmath_test, "Surface 4 #{inspect(test_surface(half, 4))}")
    Faplogger.log(:nmath_test, "Surface 5 #{inspect(test_surface(half, 5))}")
  end

  def from_vec3_list([]) do [] end
  def from_vec3_list([h | t]) do
    [from_vec3(h) | from_vec3_list(t)]
  end

  defp make_vec3({x, y, z}) do
    %NMath.Native.Vec3{x: x/1.0, y: y/1.0, z: z/1.0}
  end

  defp from_vec3(vec3) do
    {vec3.x, vec3.y, vec3.z}
  end

  defp make_vec3_list([]) do
    []
  end
  defp make_vec3_list([ {xPos, yPos, zPos}| t]) do
    [ %NMath.Native.Vec3{x: xPos, y: yPos, z: zPos} | make_vec3_list(t) ]
  end

  @doc """
  Starts the constant forces storage
  """
  @spec init() :: atom()
  def init() do
    pid = spawn(fn -> get_command([]) end)
    Process.register(pid, :cforcelist)
  end

  #def get_command([]) do
  #  receive do
  #    {:add, force} -> get_command([force | []])
  #    {:get, pid} -> send(pid, {:ok, []}); get_command([])
  #    :kill -> Process.unregister(:cforcelist)
  #  end
  #end
  def get_command(list) do
    receive do
      {:add, force} -> get_command([force | list])
      {:get, pid} -> send(pid, {:ok, list}); get_command(list)
      {:sync, pid} -> send(pid, {:sync_res, list}); get_command(list)
      {:sync_res, l} -> get_command(l)
      :kill -> Process.unregister(:cforcelist)
      o -> Faplogger.err(:nmath, "Got malformed message: #{inspect(o)}")
    end
  end

  @doc """
  Stores a constant that will be applied to the object with each call to  NMath.applyforce
  """
  @spec apply_constant_force(tuple()) :: atom()
  def apply_constant_force(force) do
    send(:cforcelist, {:add, force})
    :ok
  end

  @doc """
  Return list of the constant forces added
  """
  @spec get_constant_force() :: list(tuple())
  def get_constant_force() do
    send(:cforcelist, {:get, self()})
    receive do
      {:ok, list} -> list
      _ -> []
    end
  end
  @doc """
  Kill the constant force storage.
  """
  @spec kill_constant_force() :: atom()
  def kill_constant_force() do
    send(:cforcelist, :kill)
    :ok
  end

  def sync_constant_force() do
    send({:cforcelist, hd(Node.list())}, {:sync, {:cforcelist, Node.self()}})
  end
end
