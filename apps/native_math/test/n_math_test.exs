defmodule NMathTest do
  use ExUnit.Case
  doctest NMath

  test "add_force" do
    obj = {{:pos, {2.5, 2.5, 2.5}},
	   {:vectors, [{1.0,1.0,-1.0}, {1.0, 1.0,1.0}, {-1.0, -1.0, -1.0}]},
	   {:speed, {1.0, 0.0, 1.0}},
	   {:resultingForce, {1.0, 0.0, 0.0}}, {:type, "test"},
	   {:attributes, []}, {:rules, []}}
    
    assert NMath.add_force(obj, [{0.0, -9.82, 0.0}])
  end

  test "apply_force" do
    NMath.init()
    obj = {{:pos, {2.5, 2.5, 2.5}},
	   {:vectors, [{1.0,1.0,-1.0}, {1.0, 1.0,1.0}, {-1.0, -1.0, -1.0}]},
	   {:speed, {1.0, 0.0, 1.0}},
	   {:resultingForce, {1.0, 0.0, 0.0}}, {:type, "test"},
	   {:attributes, %{"mass" => 67.0}}, {:rules, []}}
    a = Float.round(1.0 + (1.0/67.0)/0.01, 3)
    {x, y, z} = NMath.apply_force(obj, 0.01, [])
    assert {^a, 0.0, 1.0} = {Float.round(x, 3) , Float.round(y, 1), Float.round(z, 1)}
    NMath.kill_constant_force()
  end

  test "collision boxes inside of each other" do
    obj1 = {{:pos, {2.5, 2.5, 2.5}},
	    {:vectors, [{1.0,1.0,-1.0}, {1.0, 1.0,1.0}, {-1.0, -1.0, -1.0}]},
	    {:speed, {1.0, 0.0, 1.0}},
	    {:resultingForce, {1.0, 0.0, 0.0}}, {:type, "test"},
	    {:attributes, []}, {:rules, []}}
    obj2 = {{:pos, {2.5, 2.5, 2.5}},
	    {:vectors, [{1.0,1.0,-1.0}, {1.0, 1.0,1.0}, {-1.0, -1.0, -1.0}]},
	    {:speed, {1.0, 0.0, 1.0}},
	    {:resultingForce, {1.0, 0.0, 0.0}}, {:type, "test"},
	    {:attributes, []}, {:rules, []}}
    assert {:ok, true} = NMath.check_collision(obj1, obj2)
  end

  test "collision boxes outside of each other" do
    obj1 = {{:pos, {2.5, 2.5, 2.5}},
	    {:vectors, [{1.0,1.0,-1.0}, {1.0, 1.0,1.0}, {-1.0, -1.0, -1.0}]},
  	    {:speed, {1.0, 0.0, 1.0}},
	    {:resultingForce, {1.0, 0.0, 0.0}}, {:type, "test"},
	    {:attributes, []}, {:rules, []}}
    obj2 = {{:pos, {10000.0, 1000.0, 1000.0}},
	    {:vectors, [{1.0,1.0,-1.0}, {1.0, 1.0,1.0}, {-1.0, -1.0, -1.0}]},
	    {:speed, {1.0, 0.0, 1.0}},
	    {:resultingForce, {1.0, 0.0, 0.0}}, {:type, "test"},
	    {:attributes, []}, {:rules, []}}
    assert {:ok, false} = NMath.check_collision(obj1, obj2)
  end

  test "collision with rotation" do
    obj1 = {{:pos, {3.1, 3.1, 3.1}},
	    {:vectors, [{ 1.0, 0.36602540378, 1.36602540378}, {-1.0, 0.36602540378, 1.36602540378}, {1.0, -0.36602540378, -1.36602540378}]},
	    {:speed, {1.0, 0.0, 1.0}},
	    {:resultingForce, {1.0, 0.0, 0.0}}, {:type, "test"},
	    {:attributes, []}, {:rules, []}}
    obj2 = {{:pos, {3.0, 3.0, 3.0}},
	    {:vectors, [{1.0,1.0,-1.0}, {1.0, 1.0,1.0}, {-1.0, -1.0, -1.0}]},
	    {:speed, {1.0, 0.0, 1.0}},
	    {:resultingForce, {1.0, 0.0, 0.0}}, {:type, "test"},
	    {:attributes, []}, {:rules, []}}
    assert {:ok, true} = NMath.check_collision(obj1, obj2)
  end

  test "apply constant force" do
    NMath.init()
    assert NMath.apply_constant_force({0.0, -9.82, 0.0})
    obj1 = {{:pos, {3.1, 3.1, 3.1}},
	    {:vectors, [{ 1.0, 0.36602540378, 1.36602540378}, {-1.0, 0.36602540378, 1.36602540378}, {1.0, -0.36602540378, -1.36602540378}]},
	    {:speed, {0.0, 0.0, 0.0}},
	    {:resultingForce, {1.0, 0.0, 0.0}}, {:type, "test"},
	    {:attributes, %{"mass" => 1.0}}, {:rules, []}}
    {x, y, z} = NMath.apply_force(obj1, 1.0, [])
    {1.0, -9.82, 0.0} = {x, Float.round(y, 2), z}
    NMath.kill_constant_force()
  end

  test "get normal vector for plane" do
    {p2, p1, p3} = {{0.0,0.0,0.0}, {1.0, 0.0, 0.0}, {0.0, 0.0, 1.0}}
    assert {0.0, 1.0, 0.0} = NMath.calculate_normal(p1, p2, p3)
  end

  test "get normal force for plane" do
    force = {0.0, -9.82, 0.0}
    plane = {{0.0,0.0,0.0}, {1.0, 0.0, 0.0}, {0.0, 0.0, 1.0}}
    {x, y, z} = NMath.get_normal_force(force, plane)
    y = Float.round(y, 2)
    assert {0.0, 9.82, 0.0} = {x, y, z}
  end

  test "get normal for ypos of cube" do
    half = {1.0,1.0,1.0}
    side = 4 # Side ypos
    assert {0.0, 1.0, 0.0} = NMath.test_surface(%NMath.Native.Vec3{x: 1.0, y: 1.0, z: 1.0}, 4)
  end

  test "applies normal forces correctly" do
    
  end
  
end
