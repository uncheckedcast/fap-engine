#[macro_use] extern crate rustler;
#[macro_use] extern crate rustler_codegen;
extern crate lazy_static;
extern crate nalgebra as na;
extern crate collision as co;
extern crate cgmath as cg;

use rustler::{Env, Term, NifResult, Encoder};
//use na::{Matrix3x1, Matrix3};
use std::f32;
use co::{Aabb3};
use cg::{Point3, Vector3, InnerSpace};
use co::Discrete;
use co::primitive::{Cuboid};


mod atoms {
    rustler_atoms! {
        atom ok;
        //atom error;
        atom __true__ = "true";
        atom __false__ = "false";
    }
}

rustler_export_nifs! {
    "Elixir.NMath.Native",
    [
        //("corners", 3, corners),
        //("collision_detection", 4, collision_detection),
	("addForce", 2, add_force),
	("applyForce", 4, apply_force),
        ("checkCollision", 6, check_collision),
        ("calculate_normal", 3, calculate_normal),
        ("calculate_normal_force", 2, calculate_normal_force),
        ("get_surface", 2, get_surface),
        ("get_min", 3, get_min),
        ("get_max", 3, get_max),
        ("correct_pos", 6, correct_pos)
          
    ],
    Some(on_load)
}

fn on_load(_env: Env, _info: Term) -> bool {
    true
}
#[derive(NifStruct, Default, Debug)]
#[module = "NMath.Native.Vec3"]
struct Vec3 {
    pub x: f32,
    pub y: f32,
    pub z: f32
}

fn add(this: Point3<f32>, other: Point3<f32>) -> Point3<f32> {
        Point3::new(this.x+other.x, this.y+other.y, this.z+other.z)
}

fn vec3_to_point3(vector: Vec3) -> Point3<f32> {
    Point3::new(vector.x, vector.y, vector.z) 
}

fn point3_to_vec3(point: Point3<f32>) -> Vec3 {
    Vec3{x: point.x, y: point.y, z: point.z}
}

fn check_collision<'a>(env: Env<'a>, args: &[Term<'a>]) -> NifResult<Term<'a>> {
    
    let obj1_pos = vec3_to_point3( args[0].decode().unwrap_or_default() );
    let obj1_b = vec3_to_point3(  args[1].decode().unwrap_or_default() );
    let obj1_c = vec3_to_point3(  args[2].decode().unwrap_or_default() );

    let obj2_pos = vec3_to_point3( args[3].decode().unwrap_or_default() );
    let obj2_b = vec3_to_point3( args[4].decode().unwrap_or_default() );
    let obj2_c = vec3_to_point3( args[5].decode().unwrap_or_default() );

    let obj1_aabb = Aabb3::new(add(obj1_b, obj1_pos), add(obj1_c, obj1_pos));
    let obj2_aabb = Aabb3::new(add( obj2_b, obj2_pos), add(obj2_c, obj2_pos));

    // println!("Obj1 aabb {:?}", obj1_aabb);
    // println!("Obj2 aabb {:?}", obj2_aabb);
    
    Ok((atoms::ok(), obj1_aabb.intersects(&obj2_aabb)).encode(env))
}

fn get_min<'a>(env: Env<'a>, args: &[Term<'a>]) -> NifResult<Term<'a>> {
    let obj_pos = vec3_to_point3( args[0].decode().unwrap_or_default() );
    let obj_b = vec3_to_point3(  args[1].decode().unwrap_or_default() );
    let obj_c = vec3_to_point3(  args[2].decode().unwrap_or_default() );

    let obj_aabb = Aabb3::new(add(obj_b, obj_pos), add(obj_c, obj_pos));

    // obj_aabb.min.x, obj_aabb.min.y, obj_aabb.min.z

    let x_cord: f32 = obj_aabb.min.x;
    
    let obj_min_vec3: Vec3 = Vec3{x: obj_aabb.min.x, y: obj_aabb.min.y, z: obj_aabb.min.z};
    
    Ok((obj_min_vec3).encode(env))
}

fn get_max<'a>(env: Env<'a>, args: &[Term<'a>]) -> NifResult<Term<'a>> {
    let obj_pos = vec3_to_point3( args[0].decode().unwrap_or_default() );
    let obj_b = vec3_to_point3(  args[1].decode().unwrap_or_default() );
    let obj_c = vec3_to_point3(  args[2].decode().unwrap_or_default() );

    let obj_aabb = Aabb3::new(add(obj_b, obj_pos), add(obj_c, obj_pos));

    // obj_aabb.min.x, obj_aabb.min.y, obj_aabb.min.z

    let x_cord: f32 = obj_aabb.min.x;
    
    let obj_min_vec3: Vec3 = Vec3{x: obj_aabb.max.x, y: obj_aabb.max.y, z: obj_aabb.max.z};
    
    Ok((obj_min_vec3).encode(env))
}

fn correct_pos<'a>(env: Env<'a>, args: &[Term<'a>]) -> NifResult<Term<'a>> {
    let mut obj1_pos = vec3_to_point3( args[0].decode().unwrap_or_default() );
    let obj1_b = vec3_to_point3(  args[1].decode().unwrap_or_default() );
    let obj1_c = vec3_to_point3(  args[2].decode().unwrap_or_default() );

    let obj2_pos = vec3_to_point3( args[3].decode().unwrap_or_default() );
    let obj2_b = vec3_to_point3( args[4].decode().unwrap_or_default() );
    let obj2_c = vec3_to_point3( args[5].decode().unwrap_or_default() );

    let mut obj1_aabb = Aabb3::new(add(obj1_b, obj1_pos), add(obj1_c, obj1_pos));
    let obj2_aabb = Aabb3::new(add( obj2_b, obj2_pos), add(obj2_c, obj2_pos));
    
    //println!("[RUST] posY before: {}", obj1_pos.y);
    if (obj1_pos.y > obj2_pos.y) {
        obj1_pos.y = obj2_aabb.max.y+2.0 * (obj1_pos.y - obj1_aabb.min.y);// + 0.25;
    }
    else {
        obj1_pos.y = obj2_aabb.min.y-2.0 * (obj1_pos.y - obj1_aabb.max.y);// - 0.25;
    }
    //println!("[RUST] posY after: {}", obj1_pos.y);


    obj1_aabb = Aabb3::new(add(obj1_b, obj1_pos), add(obj1_c, obj1_pos));
    if !(obj1_aabb.min.y >= obj2_aabb.max.y || obj1_aabb.max.y <= obj2_aabb.min.y) {
        if (obj1_pos.x > obj2_pos.x)  {
            obj1_pos.x = obj2_aabb.max.x-(obj1_aabb.min.x-obj1_pos.x) + 0.01;
        } else {
            obj1_pos.x = obj2_aabb.min.x+(obj1_aabb.max.x-obj1_pos.x) - 0.01;
        }
        obj1_aabb = Aabb3::new(add(obj1_b, obj1_pos), add(obj1_c, obj1_pos));
        if !(obj1_aabb.min.x >= obj2_aabb.max.x || obj1_aabb.max.x <= obj2_aabb.min.x) {
            if (obj1_pos.z > obj2_pos.z) {
                obj1_pos.z = obj2_aabb.max.z-(obj1_aabb.min.z-obj1_pos.z) + 0.01;
            } else {
                obj1_pos.z = obj2_aabb.min.z+(obj1_aabb.max.z-obj1_pos.z) - 0.01;
            }
        }
    }

    Ok(point3_to_vec3(obj1_pos).encode(env))
}

#[allow(dead_code)]
/*fn vector3_to_vec3(inv: Matrix3x1<f32>) -> Vec3{
    return Vec3{x: *inv.get(0).unwrap(), y: *inv.get(1).unwrap(), z: *inv.get(2).unwrap()}
}*/

fn vector3_to_vec3(vec: Vector3<f32>) -> Vec3 {
    return Vec3{x: vec.x, y: vec.y, z: vec.z};
}

fn vec3_to_vector3(vec: Vec3) -> Vector3<f32> {
    return Vector3::new(vec.x, vec.y, vec.z);
}

/*
 *  Takes 3 vectors and calculate the normal to a plane
 */
fn calculate_normal<'a>(env: Env<'a>, args: &[Term<'a>]) -> NifResult<Term<'a>> {
    let b = vec3_to_vector3(args[0].decode().unwrap_or_default());
    let r = vec3_to_vector3(args[1].decode().unwrap_or_default()) - b;
    let s = vec3_to_vector3(args[2].decode().unwrap_or_default()) - b;

    let n = r.cross(s);

    Ok( (atoms::ok(), vector3_to_vec3(n.normalize())).encode(env) )
}

/*
 *  Takes a force vector and a normal vector, caclulates the force parallell to the normal vector
 */

fn calculate_normal_force<'a>(env: Env<'a>, args: &[Term<'a>]) -> NifResult<Term<'a>> {
    let force = vec3_to_vector3(args[0].decode().unwrap_or_default());
    let n = vec3_to_vector3(args[1].decode().unwrap_or_default());

    let nscale = cg::dot(force, n);
    let nforce = n * -nscale;

    Ok((atoms::ok(), vector3_to_vec3(nforce)).encode(env))
}

/*
 *  collision::Cubeoid, based on one vec for halfextent. Gives all corners.
 */

fn get_cubeoid(half: Vector3<f32>) -> Cuboid<f32> {
    return Cuboid::new(half.x * 2.0, half.y * 2.0, half.z * 2.0);
}

/*enum Axis {
    XFront(0),
    YTop(1),
    ZRight(2),
    XBack(3),
    YBottom(4),
    ZLeft(5),
}*/

fn generate_corners(half_dim: &Vector3<f32>) -> [Point3<f32>; 8] {
    [
        Point3::new(half_dim.x, half_dim.y, half_dim.z),
        Point3::new(-half_dim.x, half_dim.y, half_dim.z),
        Point3::new(-half_dim.x, -half_dim.y, half_dim.z),
        Point3::new(half_dim.x, -half_dim.y, half_dim.z),
        Point3::new(half_dim.x, half_dim.y, -half_dim.z),
        Point3::new(-half_dim.x, half_dim.y, -half_dim.z),
        Point3::new(-half_dim.x, -half_dim.y, -half_dim.z),
        Point3::new(half_dim.x, -half_dim.y, -half_dim.z),
    ]
}

fn get_surface_int(half: &Vector3<f32>, axis: u8) -> [Point3<f32>; 3] {
    let cube = generate_corners(&half);
    match axis {
        0 => {
            return [cube[0], cube[4], cube[7]];
        },
        1 => {
            return [cube[4], cube[0], cube[1]];
        },
        2 => {
            return [cube[1], cube[0], cube[3]];
        },
        3 => {
            return [cube[5], cube[1], cube[2]];
        },
        4 => {
            return [cube[3], cube[7], cube[6]];
        },
        5 => {
            return [cube[7], cube[4], cube[5]];
        },
        _ => {
            return [cube[0], cube[4], cube[7]];
        }
    }
}

fn get_surface<'a>(env: Env<'a>, args: &[Term<'a>]) -> NifResult<Term<'a>> {
    let half = vec3_to_vector3(args[0].decode().unwrap_or_default());
    let axis: u8 = args[1].decode().unwrap_or_default();
    let vecs = get_surface_int(&half, axis);

    let mut out = vec![];
    out.push(point3_to_vec3(vecs[0]));
    out.push(point3_to_vec3(vecs[1]));
    out.push(point3_to_vec3(vecs[2]));

    Ok((atoms::ok(), out).encode(env))
}

fn add_force<'a>(env: Env<'a>, args: &[Term<'a>]) -> NifResult<Term<'a>> {
    // Resulting force
   let res_force: Vec3 = args[0].decode().unwrap_or_default();
   // Forces to add
   let forces: Vec<Vec3> = args[1].decode().unwrap_or_default();

   let mut added_forces: Vec3 = Vec3{x: 0.0, y: 0.0, z: 0.0} ;

   for i_force in forces {
       added_forces = Vec3{x: i_force.x + added_forces.x, y: i_force.y + added_forces.y, z: i_force.z + added_forces.z};
   }

    let ret_force = Vec3{x: added_forces.x + res_force.x, y: added_forces.y + res_force.y, z: added_forces.z + res_force.z};
   Ok((ret_force).encode(env))
}

fn apply_force<'a>(env: Env<'a>, args: &[Term<'a>]) -> NifResult<Term<'a>> {
   // Speed
   let speed: Vec3 = args[0].decode().unwrap_or_default();
   // Resulting force
   let res_force: Vec3 = args[1].decode().unwrap_or_default();
   // Mass
   let mass: f32 = args[2].decode().unwrap_or_default();
   // Delta time
   let delta_time: f32 = args[3].decode().unwrap_or_default();

   let ret_speed: Vec3 = Vec3{
        x: speed.x + ((res_force.x/mass)/delta_time),
        y: speed.y + ((res_force.y/mass)/delta_time),
        z: speed.z + ((res_force.z/mass)/delta_time)};

   Ok((ret_speed).encode(env))
}
