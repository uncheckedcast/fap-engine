defmodule Evaltool do
  require Evaluator
  
  @moduledoc """
  Documentation for Evaltool.
  """

  def parse_check([]) do [] end
  def parse_check([h|t]) do
    l = [Evaluator.Parser.parse(h)|rule_list_parser(t)]
    undef_check(l,1)
  end

  def undef_check([],_) do :ok end
  def undef_check([h|t] = l,n) do
    hl = Tuple.to_list(h)
    if(Enum.member?(hl,:undef)) do
      IO.write("\n Rule ##{inspect(n)}: Invalid\n")
    else
      IO.write("\n Rule ##{inspect(n)}: Valid\n")
    end
    undef_check(t,n+1)
  end
  
  def parse_file(args) do
    case File.read(args) do
      {:ok, body} ->
	parse_check(body)
      {:error,error} ->
	IO.write("\n Error as follows \n #{inspect(error)}\n")
    end
  end

  def rule_list_parser([]) do [] end
  def rule_list_parser([h|t] = _rules) do
    [Evaluator.Parser.parse(h)|rule_list_parser(t)]
  end
  
  
  def parse_rules(obj) do
    {{:pos, {_xPos, _yPos, _zPos}},
     {:vectors, _posVectors},
     {:speed, {_xVel, _yVel, _zVel}},
     {:resultingForce, {_xFor, _yFor, _zFor}}, {:type, _objType},
     {:attributes, _attributes}, {:rules, rules}} = obj
    rule_list_parser(rules)
  end

  def help() do
    IO.write("h for help\nrot to read this object from file\nroo to read other object from file\ne2 to evaluate using both objects\ne1 to evaluate using this object\n")
  end

  def read_object(filepath) do
    case IO.read(filepath) do
      {:ok, body} ->
	body
      {:error, error} ->
	IO.write("#{inspect(error)}\n")
    end
  end
  
  def cli({{this,thisrules},{other,otherrules}}) do
    {{nthis,nrules},{nother,norules}} = case IO.gets("Enter Command\n") do
					 "h\n" ->
					   help()
					   {{this,thisrules},{other,otherrules}}
					 "rot\n" ->
					   new_this = read_object(IO.gets("enter this object file name\n"))
					   
					   {{new_this,parse_rules(new_this)}, {other,otherrules}}
					 "roo\n" ->
					   new_other = read_object(IO.gets("enter other object file name\n"))
					   {{this,thisrules},{new_other,parse_rules(new_other)}}
					 "e2\n" ->
					   if(this == [] || other == []) do
					     IO.write("No objects entered\n")
					     {{this,thisrules},{other,otherrules}}
					   else
					     type = String.to_atom(IO.gets("enter type of evaluation\n"))
					     IO.write("resulting environment\n#{inspect(Evaluator.eval_rules(thisrules,Evaluator.Parser.parse_env([this,other]),type))}\n")
					     {{this,thisrules},{other,otherrules}}
					   end
					 "e1\n" ->
					   type = String.to_atom(IO.gets("enter type of evaluation\n"))
					   if(this == []) do
					     IO.write("No object entered\n")
					     {{this,thisrules},{other,otherrules}}
					   else
					     IO.write("resulting environment\n#{inspect(Evaluator.eval_rules(thisrules,Evaluator.Parser.parse_env([this]),type))}\n")
					     {{this,thisrules},{other,otherrules}}
					   end
				       end
    cli({{nthis,nrules},{nother,norules}})
  end
end
