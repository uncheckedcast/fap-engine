#User story - Implementing the core commands for the server
#Messages will have the following base structure:
#"{
#   "server": true,
#   "command": "command-type",
#   "args": {
#             'Insert specific command here'
#           }
#}"
#--------------------------------------------------------------------------------
defmodule Commands do
  require Routing
  require Store
  require Scheduler
  require Evallist
  require NMath
  require Faplogger

  @doc """
  Method deserialize/0 retrieves JSON encoded data from a process
  Expected outcome is a status atom from the Routing module
  """
  @spec deserialize() :: any()
  def deserialize() do
    ##Decode data,
    receive do
      {:cmd, data} ->
        {status, list} = JSON.decode(data)
        case status do
          ##Check status message from the JSON_decoder
          :ok ->
            args = list["args"]
            case list["command"] do
              "connect" ->
                bitconnect(args)
              "addObj" ->
                addObj(args)
              "removeObj" ->
                removeObj(args)
              "updateObj" ->
				updateObj(args)
              "setSpeed" ->
				setSpeed(args)
              "getWorld" ->
				getWorld()
              "startLobby" ->
				startLobby(args)
			  "chat" ->
				chat(args)
			  "disconnect" ->
				disconnect()
			  "updatePos" ->
				updatePos(args)
			  "updateVecs" ->
				updateVecs(args)
			  "updateAttributes" ->
				updateAttributes(args)
			  "updateForce" ->
				updateForce(args)
			  "setGravity" ->
				setGravity(args)
              _ ->
				:error_incorrect_commandtype
            end

	    _ ->
          :error_failed_to_deserialize_data
	  end
    {:store_update, uuid, obj} ->
      case Evaluator.Parser.is_obj(obj) do
        true ->
          Routing.to_client(serialize([id: uuid, obj: depars_Obj(obj)], "updateObj"))
        false ->
          Routing.to_client(serialize([id: uuid, obj: obj], "updateObj"))
      end
	  {:store_add, uuid, obj} ->
      case Evaluator.Parser.is_obj(obj) do
        true ->
          Routing.to_client(serialize([seq: 0, id: uuid, obj: depars_Obj(obj)], "addObj"))
        false ->
          Routing.to_client(serialize([seq: 0, id: uuid, obj: obj], "addObj"))
      end
	  {:store_remove, uuid} ->
		Routing.to_client(serialize([id: uuid], "removeObj"))
          {:json, data} ->
            Routing.to_client(data)
      o -> Faplogger.err(:command, "Got malformed message: #{inspect(o)}")
    end
    deserialize()
  end

  @doc """
  Method serialize/2 takes data and command as arguments
  Expected outcome is a correctly formatted JSON-message
  """
  @spec serialize(list(), String.t()) :: String.t()
  def serialize(data, command) do
    message = [server: true, command: command, args: data]
    {status, result} = JSON.encode(message)
    case status do
      :ok ->
        result
      _ ->
        :error_failed_serialize_data
    end
  end

  ####Core-commands
  
  #Method bitconnect/1 connects to lobby on the given args myIP and IP
  @spec bitconnect(list()) :: atom()
  def bitconnect(args) do
    ##Server false
    ##Command: "connect"
    ##:gen_udp.open(8888, [:binary, active: true])
    ipaddr = args["ip"]
    myip = args["myip"]
    Routing.connect(myip, ipaddr)
    Store.sync_world()
    NMath.sync_constant_force()
    Routing.broadcast({:connect})
  end

  #Method addObj/1 adds an object to the world on the server
  @spec addObj(list()) :: atom()
  def addObj(args) do
    ##Server: true
    ##Command: "addObj"
    seq = args["seq"]
    obj = pars_Obj(args["obj"])
    objType = args["obj"]["type"]
    ##Generate UUID, send to store, send to other clients
    uuid = Store.ref_to_string(make_ref())
    case Store.add_obj(uuid, objType, obj) do
      :ok ->
		:ok
		  if(seq != 0) do
			send_to_client(4 ,serialize([seq: seq, id: uuid, obj: depars_Obj(obj)], "addObj"))
		  end
        Routing.to_client(serialize([seq: seq, id: uuid, obj: depars_Obj(obj)], "addObj"))
      {:error, _} ->
        :error_adding_object_to_store
    end
  end
  
  def send_to_client(0, message) do
	Routing.to_client(message)
  end
  
  def send_to_client(num, message) do
	Routing.to_client(message)
	send_to_client(num-1, message)
  end
	
	
	
  #Method updateObj/1 updates an object in the world on the server
  def updateObj(args) do
    ##Server: true
    ##Command: "updateObj"
    id = args["id"]
    obj = pars_Obj(args["obj"])
    case Store.update_obj(id, obj) do
      :ok ->
		:ok
      {:error, _} ->
        :error_updating_object_in_store
    end
  end

  #Method removeObj/1 removes object from the world on the server
  def removeObj(args) do
    ##Server: true
    ##Command: "removeObj"
    id = args["id"]
    case Store.remove_obj(id) do
      :ok ->
		:ok
        #Response {"removed", id}
      {:error, _} ->
        :error_removing_object_from_store
    end
  end

  #Method setSpeed/1 with infinite acceleration, that is, instant speed.
  def setSpeed(args) do
    ##Server: true
    ##Command: "setSpeed"
    id = args["id"]
    speed = args["speed"]

    case Store.get_obj(id) do
      {:ok, obj} ->
        {{:pos, {xPos, yPos, zPos}},
         {:vectors, [{x0,y0,z0}, {x1, y1, z1}, {x2, y2, z2}]},
         {:speed, {_xVel, _yVel, _zVel}},
         {:resultingForce, {xFor, yFor, zFor}}, {:type, objType},
         {:attributes, attributes}, {:rules, rules}} = obj
        ##Modify the speed of the object
        xSpeed = speed["x"]
        ySpeed = speed["y"]
        zSpeed = speed["z"]
        obj = {{:pos, {xPos, yPos, zPos}},
               {:vectors, [{x0,y0,z0}, {x1, y1, z1}, {x2, y2, z2}]},
               {:speed, {xSpeed, ySpeed, zSpeed}},
               {:resultingForce, {xFor, yFor, zFor}}, {:type, objType},
               {:attributes, attributes}, {:rules, rules}}
        ##Update the object with correct speed
        case Store.update_obj(id, obj) do
          {:ok, _} ->
	    :ok
            ##Routing.broadcast({:json, serialize([speed: speed, id: id], "setSpeed")})
            _ ->
            :error_updating_object_in_store
        end
      {:error, _} ->
        :error_retrieving_object_from_store
    end
  end

  #Method getWorld/0 retrieves the world saved in store
  def getWorld() do
    ##Server: true
    ##Command: "getWorld"
    {status, world} = Store.get_world()
    case  status do
      :ok ->
		pars_World(world)
      _ ->
        :error_retrieving_world_from_store
    end
  end

  #Method startLobby/1 that starts up a lobby
  def startLobby(args) do
    myip = args["myip"]
    Routing.start_lobby(myip)
  end





  ###################################################################################

  ####Some utility functions

  #Method init/0 that starts routing and store, NOT USED AS A CORE-COMMAND. Used for initializing!
  def init() do
    Process.register(spawn(fn -> deserialize() end), :command)
    Faplogger.init()
    case Routing.start_routing() do
      :true ->
                Faplogger.log(:init, "Routing Started")
		case Store.init() do
		  :true ->
                        Faplogger.log(:init, "Store Started")
			case Evallist.init() do
			  :true ->
                                Faplogger.log(:init, "Evallist Started")
				case NMath.init() do
				  :true ->
                                        Faplogger.log(:init, "NMath Started")
					case Scheduler.init() do
					  :true ->
                                            Faplogger.log(:init, "Scheduler Started")
                                            :true
					  _ ->
                                            Faplogger.err(:init, "Error initializing Scheduler")
                                            :error_initializing_Scheduler
					end
				  _ ->
                                    Faplogger.err(:init, "Error initializing NMath")
                                    :error_initializing_NMath
				end
			  _ ->
                            Faplogger.err(:init, "Error initializing Evallist")
                            :error_initializing_Evallist
			end
		  _ ->
                    Faplogger.err(:init, "Error initializing Store")
                    :error_initialising_Store
		end
	  _ ->
            Faplogger.err(:init, "Error initializing Routing")
            :error_starting_Routing
    end
    loopy()
  end

  def loopy() do
    receive do
      _ ->
        :ok
    end
    loopy()
  end

  def pars_World([{uuid,obj}|[]]) do
	Routing.to_client(serialize([last: true, id: uuid, obj: depars_Obj(obj)], "getWorld"))
  end

  def pars_World([{uuid,obj}|world]) do
	Routing.to_client(serialize([last: false, id: uuid, obj: depars_Obj(obj)], "getWorld"))
	pars_World(world)
  end

  def pars_World([]) do
    []
  end

  def pars_Obj(obj) do
    %{"attributes" => attributes,
      "pos" => %{"x" => xPos,"y" => yPos, "z" => zPos},
      "resultingForce" => %{"x" => xFor, "y" => yFor, "z" => zFor},
      "rules" => rules,
      "speed" =>%{"x" => xSpeed, "y" => ySpeed, "z" => zSpeed},
      "type" => objType,
      "vecs" => [%{"x" => x0, "y" => y0, "z" => z0},
		 %{"x" => x1, "y" => y1, "z" => z1},
		 %{"x" => x2, "y" => y2, "z" => z2}
		]} = obj

    rules = cond do
      is_binary(rules) ->
		[]
      true ->
		rules
    end

    {{:pos, {xPos, yPos, zPos}},
     {:vectors, [{x0,y0,z0}, {x1, y1, z1}, {x2, y2, z2}]},
     {:speed, {xSpeed, ySpeed, zSpeed}},
     {:resultingForce, {xFor, yFor, zFor}}, {:type, objType},
     {:attributes, attributes}, {:rules, rules}}
  end

  def depars_Obj(obj) do
    {{:pos, {xPos, yPos, zPos}},
     {:vectors, [{x0,y0,z0}, {x1, y1, z1}, {x2, y2, z2}]},
     {:speed, {xSpeed, ySpeed, zSpeed}},
     {:resultingForce, {xFor, yFor, zFor}}, {:type, objType},
     {:attributes, attributes}, {:rules, rules}} = obj

	xPos = Float.round(xPos,5)
	yPos = Float.round(yPos,5)
	zPos = Float.round(zPos,5)
	xFor = Float.round(xFor,5)
	yFor = Float.round(yFor,5)
	zFor = Float.round(zFor,5)
	xSpeed = Float.round(xSpeed,5)
	ySpeed = Float.round(ySpeed,5)
	zSpeed = Float.round(zSpeed,5)
	x0 = Float.round(x0,5)
	y0 = Float.round(y0,5)
	z0 = Float.round(z0,5)
	x1 = Float.round(x1,5)
	y1 = Float.round(y1,5)
	z1 = Float.round(z1,5)
	x2 = Float.round(x2,5)
	y2 = Float.round(y2,5)
	z2 = Float.round(z2,5)


    %{"attributes" => attributes,
      "pos" => %{"x" => xPos,"y" => yPos, "z" => zPos},
      "resultingForce" => %{"x" => xFor, "y" => yFor, "z" => zFor},
      "rules" => rules,
      "speed" =>%{"x" => xSpeed, "y" => ySpeed, "z" => zSpeed},
      "type" => objType,
      "vecs" => [%{"x" => x0, "y" => y0, "z" => z0},
	  %{"x" => x1, "y" => y1, "z" => z1},
	  %{"x" => x2, "y" => y2, "z" => z2}
	]}
  end
  ###################################################################################

  ####Extended-commands

  #Method chat/1 broadcasts a message
  def chat(args) do
	Routing.to_client(serialize(args, "chat"))
	Routing.broadcast_n(:command, {:json, serialize(args, "chat")})
  end

  #Method disconnect/0 disconnects the client
  def disconnect() do
	Routing.disconnect()
  end

  #Method updateAttributes/1 updates the attributes of an object in the store
  def updateAttributes(args) do
	uuid = args["id"]
	attributes = args["attributes"]
	Store.update_attributes(uuid, attributes)
  end

  #Method updatePos/1 updates the position of an object in the store
  def updatePos(args) do
	uuid = args["id"]
	pos = {args["pos"]["x"], args["pos"]["y"], args["pos"]["z"]}
	Store.update_pos(uuid, pos)
  end

  #Method updateVecs/1 updates the vectors of an object in the store
  def updateVecs(args) do
	uuid = args["id"]
	[%{"x" => x0, "y" => y0, "z" => z0},
		 %{"x" => x1, "y" => y1, "z" => z1},
		 %{"x" => x2, "y" => y2, "z" => z2}
		] = args["vecs"]
	vecs = [{x0,y0,z0}, {x1, y1, z1}, {x2, y2, z2}]
        #IO.write("Updating vecs\n\t#{inspect(uuid)}\n\t#{inspect(vecs)}\n")
	Store.update_vecs(uuid, vecs)
  end

  #Method updateForce/1 updates the resultingForce of an object in the store
  def updateForce(args) do
    uuid = args["id"]
  	%{"x" => xFor, "y" => yFor, "z" => zFor} = args["resultingForce"]
  	force = {xFor, yFor, zFor}
  	Store.update_force(uuid, force)
  end

  #Method setGravity/1 sets the gravity for the world
  def setGravity(args) do
    %{"x" => xGrav, "y" => yGrav, "z" => zGrav} = args["gravity"]
    gravity = {xGrav, yGrav, zGrav}
    Faplogger.log(:setGravity, "#{inspect(gravity)}")
    NMath.apply_constant_force(gravity)
  end
end
#--------------------------------------------------------------------------------
