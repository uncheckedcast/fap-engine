defmodule CommandsTest do
  use ExUnit.Case
  doctest Commands

  #PASS
  #Hardcoded for kapitalisthotspot, Ågrens dator & connecta till fredrik
  #test "connect to lobby" do
  #  assert Commands.deserialize("
  #  {
  #  \"server\": true,
  #  \"command\": \"connect\",
  #  \"args\": {\"ip\": \"192.168.43.120\", \"myip\": \"192.168.43.168\"}
  #  }
  #  ")
  #end

  #PASS
  test "add an object" do
    assert Commands.deserialize("{\"server\":true,\"command\":\"addObj\",\"args\":{\"seq\":129,\"obj\":{}}}") == nil
  end

  #test "update an object" do
  #  assert Commands.deserialize("{\"server\":true,\"command\":\"updateObj\",\"args\":{\"id\":\"129\",\"obj\":{\"x\":2}}}")
  #end

  #PASS
  test "remove an object" do
    assert Commands.deserialize("{
    \"server\": true,
    \"command\": \"removeObj\",
    \"args\": {
        \"id\": \"129\",
    }
}")
  end

  #PASS
  test "set speed of an object" do
    assert Commands.deserialize("{
    \"server\": true,
    \"command\": \"setSpeed\",
    \"args\": {
        \"id\": \"129\",
        \"speed\": {\"x\":1,\"y\":2,\"z\":3}
    }
}")
  end

  #PASS
  test "get the entire world" do
    assert Commands.deserialize("{
    \"server\": true,
    \"command\": \"getWorld\",
    \"args\": {}}")
  end

  #PASS
  #test "start lobby" do
  #  assert Commands.deserialize("{\"server\": true,
  #  \"command\": \"startLobby\"
  #  \"args\":{\"myip\": \"192.168.43.168\"}}")
  #end

  #PASS
  test "serialize data" do
    assert Commands.serialize([obj: [x: 2], id: 10], "updateObj") == "{\"server\":true,\"command\":\"updateObj\",\"args\":{\"obj\":{\"x\":2},\"id\":10}}"
  end
end
